//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class WarehouseOrderPreparingApi {
  WarehouseOrderPreparingApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// assign this packages delivery job to current user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<Response> orderPreparingAssignDeliveryToMePostWithHttpInfo({ AssignRequest assignRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/AssignDeliveryToMe';

    Object postBody = assignRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// assign this packages delivery job to current user
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<OrderBR> orderPreparingAssignDeliveryToMePost({ AssignRequest assignRequest }) async {
    final response = await orderPreparingAssignDeliveryToMePostWithHttpInfo( assignRequest: assignRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderBR') as OrderBR;
        }
    return Future<OrderBR>.value(null);
  }

  /// assign given delivery job to an user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AssignToOtherRequest] assignToOtherRequest:
  Future<Response> orderPreparingAssignDeliveryToUserPostWithHttpInfo({ AssignToOtherRequest assignToOtherRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/AssignDeliveryToUser';

    Object postBody = assignToOtherRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// assign given delivery job to an user
  ///
  /// Parameters:
  ///
  /// * [AssignToOtherRequest] assignToOtherRequest:
  Future<OrderBR> orderPreparingAssignDeliveryToUserPost({ AssignToOtherRequest assignToOtherRequest }) async {
    final response = await orderPreparingAssignDeliveryToUserPostWithHttpInfo( assignToOtherRequest: assignToOtherRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderBR') as OrderBR;
        }
    return Future<OrderBR>.value(null);
  }

  /// assign the pakcaging job to current user
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<Response> orderPreparingAssignPackagingToMePostWithHttpInfo({ AssignRequest assignRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/AssignPackagingToMe';

    Object postBody = assignRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// assign the pakcaging job to current user
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<OrderBR> orderPreparingAssignPackagingToMePost({ AssignRequest assignRequest }) async {
    final response = await orderPreparingAssignPackagingToMePostWithHttpInfo( assignRequest: assignRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderBR') as OrderBR;
        }
    return Future<OrderBR>.value(null);
  }

  /// assaign the packagint to an user  only supervisor accoutns can call this
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AssignToOtherRequest] assignToOtherRequest:
  Future<Response> orderPreparingAssignPackagingToUserPostWithHttpInfo({ AssignToOtherRequest assignToOtherRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/AssignPackagingToUser';

    Object postBody = assignToOtherRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// assaign the packagint to an user  only supervisor accoutns can call this
  ///
  /// Parameters:
  ///
  /// * [AssignToOtherRequest] assignToOtherRequest:
  Future<OrderBR> orderPreparingAssignPackagingToUserPost({ AssignToOtherRequest assignToOtherRequest }) async {
    final response = await orderPreparingAssignPackagingToUserPostWithHttpInfo( assignToOtherRequest: assignToOtherRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderBR') as OrderBR;
        }
    return Future<OrderBR>.value(null);
  }

  /// Returns the order if the user is assigned to any order right now  Returns null if the user is unoccupied  Call this when starting the delivery app to see if the user has any packages assigned to them  If they do redirect them accordingly
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> orderPreparingGetCurrentOrderPostWithHttpInfo() async {
    final path = r'/orderPreparing/GetCurrentOrder';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Returns the order if the user is assigned to any order right now  Returns null if the user is unoccupied  Call this when starting the delivery app to see if the user has any packages assigned to them  If they do redirect them accordingly
  Future<OrderBR> orderPreparingGetCurrentOrderPost() async {
    final response = await orderPreparingGetCurrentOrderPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderBR') as OrderBR;
        }
    return Future<OrderBR>.value(null);
  }

  /// Returns delivery history for the current user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> orderPreparingGetDeliveryHistoryPostWithHttpInfo() async {
    final path = r'/orderPreparing/GetDeliveryHistory';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Returns delivery history for the current user
  Future<OrderListBR> orderPreparingGetDeliveryHistoryPost() async {
    final response = await orderPreparingGetDeliveryHistoryPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderListBR') as OrderListBR;
        }
    return Future<OrderListBR>.value(null);
  }

  /// Get order from Id
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [OrderIdRequest] orderIdRequest:
  Future<Response> orderPreparingGetOrderFromIdPostWithHttpInfo({ OrderIdRequest orderIdRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/GetOrderFromId';

    Object postBody = orderIdRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Get order from Id
  ///
  /// Parameters:
  ///
  /// * [OrderIdRequest] orderIdRequest:
  Future<OrderBR> orderPreparingGetOrderFromIdPost({ OrderIdRequest orderIdRequest }) async {
    final response = await orderPreparingGetOrderFromIdPostWithHttpInfo( orderIdRequest: orderIdRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderBR') as OrderBR;
        }
    return Future<OrderBR>.value(null);
  }

  /// Returns orders ready for delivery for the current user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> orderPreparingGetOrdersForDeliveryPostWithHttpInfo() async {
    final path = r'/orderPreparing/GetOrdersForDelivery';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Returns orders ready for delivery for the current user
  Future<OrderListBR> orderPreparingGetOrdersForDeliveryPost() async {
    final response = await orderPreparingGetOrdersForDeliveryPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderListBR') as OrderListBR;
        }
    return Future<OrderListBR>.value(null);
  }

  /// returns orders that needs to be packaged for the current user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> orderPreparingGetOrdersForPreparingPostWithHttpInfo() async {
    final path = r'/orderPreparing/GetOrdersForPreparing';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns orders that needs to be packaged for the current user
  Future<OrderListBR> orderPreparingGetOrdersForPreparingPost() async {
    final response = await orderPreparingGetOrdersForPreparingPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderListBR') as OrderListBR;
        }
    return Future<OrderListBR>.value(null);
  }

  /// Returns package history for the current user
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> orderPreparingGetPackagingHistoryPostWithHttpInfo() async {
    final path = r'/orderPreparing/GetPackagingHistory';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Returns package history for the current user
  Future<OrderListBR> orderPreparingGetPackagingHistoryPost() async {
    final response = await orderPreparingGetPackagingHistoryPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'OrderListBR') as OrderListBR;
        }
    return Future<OrderListBR>.value(null);
  }

  /// set the order as delivered
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<Response> orderPreparingOrderDeliveredPostWithHttpInfo({ AssignRequest assignRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/OrderDelivered';

    Object postBody = assignRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// set the order as delivered
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<BR> orderPreparingOrderDeliveredPost({ AssignRequest assignRequest }) async {
    final response = await orderPreparingOrderDeliveredPostWithHttpInfo( assignRequest: assignRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }

  /// sets the order as ready for delivery
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<Response> orderPreparingReadyForDeliveryPostWithHttpInfo({ AssignRequest assignRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/ReadyForDelivery';

    Object postBody = assignRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// sets the order as ready for delivery
  ///
  /// Parameters:
  ///
  /// * [AssignRequest] assignRequest:
  Future<BR> orderPreparingReadyForDeliveryPost({ AssignRequest assignRequest }) async {
    final response = await orderPreparingReadyForDeliveryPostWithHttpInfo( assignRequest: assignRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }

  /// Sets last known delivery location for order
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [OrderDeliveryLocationRequest] orderDeliveryLocationRequest:
  Future<Response> orderPreparingSetOrderDeliveryLastLocationPostWithHttpInfo({ OrderDeliveryLocationRequest orderDeliveryLocationRequest }) async {
    // Verify required params are set.

    final path = r'/orderPreparing/SetOrderDeliveryLastLocation';

    Object postBody = orderDeliveryLocationRequest;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json-patch+json', 'application/json', 'text/json', 'application/_*+json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Sets last known delivery location for order
  ///
  /// Parameters:
  ///
  /// * [OrderDeliveryLocationRequest] orderDeliveryLocationRequest:
  Future<BR> orderPreparingSetOrderDeliveryLastLocationPost({ OrderDeliveryLocationRequest orderDeliveryLocationRequest }) async {
    final response = await orderPreparingSetOrderDeliveryLastLocationPostWithHttpInfo( orderDeliveryLocationRequest: orderDeliveryLocationRequest );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }
}
