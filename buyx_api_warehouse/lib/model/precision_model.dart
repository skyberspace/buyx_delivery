//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class PrecisionModel {
  /// Returns a new [PrecisionModel] instance.
  PrecisionModel({
    this.isFloating,
    this.maximumSignificantDigits,
    this.scale,
    this.precisionModelType,
  });

  bool isFloating;

  int maximumSignificantDigits;

  double scale;

  PrecisionModels precisionModelType;

  @override
  bool operator ==(Object other) => identical(this, other) || other is PrecisionModel &&
     other.isFloating == isFloating &&
     other.maximumSignificantDigits == maximumSignificantDigits &&
     other.scale == scale &&
     other.precisionModelType == precisionModelType;

  @override
  int get hashCode =>
    (isFloating == null ? 0 : isFloating.hashCode) +
    (maximumSignificantDigits == null ? 0 : maximumSignificantDigits.hashCode) +
    (scale == null ? 0 : scale.hashCode) +
    (precisionModelType == null ? 0 : precisionModelType.hashCode);

  @override
  String toString() => 'PrecisionModel[isFloating=$isFloating, maximumSignificantDigits=$maximumSignificantDigits, scale=$scale, precisionModelType=$precisionModelType]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (isFloating != null) {
      json[r'isFloating'] = isFloating;
    }
    if (maximumSignificantDigits != null) {
      json[r'maximumSignificantDigits'] = maximumSignificantDigits;
    }
    if (scale != null) {
      json[r'scale'] = scale;
    }
    if (precisionModelType != null) {
      json[r'precisionModelType'] = precisionModelType;
    }
    return json;
  }

  /// Returns a new [PrecisionModel] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static PrecisionModel fromJson(Map<String, dynamic> json) => json == null
    ? null
    : PrecisionModel(
        isFloating: json[r'isFloating'],
        maximumSignificantDigits: json[r'maximumSignificantDigits'],
        scale: json[r'scale'],
        precisionModelType: PrecisionModels.fromJson(json[r'precisionModelType']),
    );

  static List<PrecisionModel> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <PrecisionModel>[]
      : json.map((v) => PrecisionModel.fromJson(v)).toList(growable: true == growable);

  static Map<String, PrecisionModel> mapFromJson(Map<String, dynamic> json) {
    final map = <String, PrecisionModel>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = PrecisionModel.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of PrecisionModel-objects as value to a dart map
  static Map<String, List<PrecisionModel>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<PrecisionModel>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = PrecisionModel.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

