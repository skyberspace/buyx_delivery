import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
//import 'package:buyx_delivery/screens/warehouse/wOrderDetails.dart';

// ignore: must_be_immutable
class ScanBarcode extends StatefulWidget {
  ScanBarcode({this.productBarcode, this.productId, this.validated});
  int productId;
  String productBarcode;
  Function validated;
  @override
  _ScanBarcodeState createState() => _ScanBarcodeState();
}

class _ScanBarcodeState extends State<ScanBarcode> {
  String _scanBarcode = 'Unknown';
  final editControllerBarcode = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  Future<void> startBarcodeScanStream() async {
    FlutterBarcodeScanner.getBarcodeStreamReceiver(
            '#ff6666', 'Cancel', true, ScanMode.BARCODE)
        .listen((barcode) => print(barcode));
  }

  Future<void> scanQR() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _scanBarcode = barcodeScanRes;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> scanBarcodeNormal() async {
    String barcodeScanRes;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.BARCODE);

      print(barcodeScanRes);
    } on PlatformException {
      barcodeScanRes = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      try {
        _scanBarcode = barcodeScanRes;
        if (widget.productBarcode == _scanBarcode) widget.validated();
      } catch (e) {
        snackBar("Unknown Problem. Please try again");
      }
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 12,
            width: MediaQuery.of(context).size.width / 1.5,
            color: Colors.white,
            child: TextButton(
              onPressed: () {},
              child: TextField(
                decoration: InputDecoration(
                    icon: IconButton(
                      onPressed: () {
                        setState(() {
                          _scanBarcode = editControllerBarcode.text;
                          if (widget.productBarcode == _scanBarcode)
                            widget.validated();
                          Navigator.pop(context);
                        });
                      },
                      icon: Icon(Icons.add),
                      color: Colors.redAccent,
                    ),
                    border: OutlineInputBorder(),
                    hintText: 'Enter barcode manually'),
                controller: editControllerBarcode,
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () => scanBarcodeNormal(),
            child: Text('Start barcode scan'),
          ),

          /*ElevatedButton(
              onPressed: () => scanQR(), child: Text('Start QR scan')),
          ElevatedButton(
              onPressed: () => startBarcodeScanStream(),
              child: Text('Start barcode scan stream')),*/
        ]);
  }

  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }
}
