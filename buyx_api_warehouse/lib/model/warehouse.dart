//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Warehouse {
  /// Returns a new [Warehouse] instance.
  Warehouse({
    this.id,
    this.name,
    this.isDefault,
    this.latitude,
    this.longitude,
    this.pricePerKM,
    this.warehouseStocks,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String name;

  bool isDefault;

  double latitude;

  double longitude;

  double pricePerKM;

  List<ProductWarehouseStock> warehouseStocks;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Warehouse &&
     other.id == id &&
     other.name == name &&
     other.isDefault == isDefault &&
     other.latitude == latitude &&
     other.longitude == longitude &&
     other.pricePerKM == pricePerKM &&
     other.warehouseStocks == warehouseStocks &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (isDefault == null ? 0 : isDefault.hashCode) +
    (latitude == null ? 0 : latitude.hashCode) +
    (longitude == null ? 0 : longitude.hashCode) +
    (pricePerKM == null ? 0 : pricePerKM.hashCode) +
    (warehouseStocks == null ? 0 : warehouseStocks.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'Warehouse[id=$id, name=$name, isDefault=$isDefault, latitude=$latitude, longitude=$longitude, pricePerKM=$pricePerKM, warehouseStocks=$warehouseStocks, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (isDefault != null) {
      json[r'isDefault'] = isDefault;
    }
    if (latitude != null) {
      json[r'latitude'] = latitude;
    }
    if (longitude != null) {
      json[r'longitude'] = longitude;
    }
    if (pricePerKM != null) {
      json[r'pricePerKM'] = pricePerKM;
    }
    if (warehouseStocks != null) {
      json[r'warehouseStocks'] = warehouseStocks;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [Warehouse] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Warehouse fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Warehouse(
        id: json[r'id'],
        name: json[r'name'],
        isDefault: json[r'isDefault'],
        latitude: json[r'latitude'],
        longitude: json[r'longitude'],
        pricePerKM: json[r'pricePerKM'],
        warehouseStocks: ProductWarehouseStock.listFromJson(json[r'warehouseStocks']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<Warehouse> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Warehouse>[]
      : json.map((v) => Warehouse.fromJson(v)).toList(growable: true == growable);

  static Map<String, Warehouse> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Warehouse>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Warehouse.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Warehouse-objects as value to a dart map
  static Map<String, List<Warehouse>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Warehouse>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Warehouse.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

