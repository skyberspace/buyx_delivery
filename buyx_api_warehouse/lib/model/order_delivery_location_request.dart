//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class OrderDeliveryLocationRequest {
  /// Returns a new [OrderDeliveryLocationRequest] instance.
  OrderDeliveryLocationRequest({
    this.orderId,
    this.longutide,
    this.latitude,
  });

  int orderId;

  double longutide;

  double latitude;

  @override
  bool operator ==(Object other) => identical(this, other) || other is OrderDeliveryLocationRequest &&
     other.orderId == orderId &&
     other.longutide == longutide &&
     other.latitude == latitude;

  @override
  int get hashCode =>
    (orderId == null ? 0 : orderId.hashCode) +
    (longutide == null ? 0 : longutide.hashCode) +
    (latitude == null ? 0 : latitude.hashCode);

  @override
  String toString() => 'OrderDeliveryLocationRequest[orderId=$orderId, longutide=$longutide, latitude=$latitude]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (orderId != null) {
      json[r'orderId'] = orderId;
    }
    if (longutide != null) {
      json[r'longutide'] = longutide;
    }
    if (latitude != null) {
      json[r'latitude'] = latitude;
    }
    return json;
  }

  /// Returns a new [OrderDeliveryLocationRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static OrderDeliveryLocationRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : OrderDeliveryLocationRequest(
        orderId: json[r'orderId'],
        longutide: json[r'longutide'],
        latitude: json[r'latitude'],
    );

  static List<OrderDeliveryLocationRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <OrderDeliveryLocationRequest>[]
      : json.map((v) => OrderDeliveryLocationRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, OrderDeliveryLocationRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, OrderDeliveryLocationRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = OrderDeliveryLocationRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of OrderDeliveryLocationRequest-objects as value to a dart map
  static Map<String, List<OrderDeliveryLocationRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<OrderDeliveryLocationRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = OrderDeliveryLocationRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

