# buyx_api_warehouse.api.WarehouseWarehouseUserApi

## Load the API package
```dart
import 'package:buyx_api_warehouse/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**warehouseUserSetFirebaseCloudMessagingTokenPost**](WarehouseWarehouseUserApi.md#warehouseusersetfirebasecloudmessagingtokenpost) | **POST** /warehouseUser/SetFirebaseCloudMessagingToken | Sets warehouse users Firebase Cloud Messaging Token


# **warehouseUserSetFirebaseCloudMessagingTokenPost**
> BooleanBR warehouseUserSetFirebaseCloudMessagingTokenPost(token)

Sets warehouse users Firebase Cloud Messaging Token

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseWarehouseUserApi();
final token = token_example; // String | 

try { 
    final result = api_instance.warehouseUserSetFirebaseCloudMessagingTokenPost(token);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseWarehouseUserApi->warehouseUserSetFirebaseCloudMessagingTokenPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**|  | [optional] 

### Return type

[**BooleanBR**](BooleanBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

