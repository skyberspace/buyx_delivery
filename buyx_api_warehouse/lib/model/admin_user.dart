//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AdminUser {
  /// Returns a new [AdminUser] instance.
  AdminUser({
    this.id,
    this.permissions,
    this.userId,
    this.user,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  Permissions permissions;

  int userId;

  User user;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AdminUser &&
     other.id == id &&
     other.permissions == permissions &&
     other.userId == userId &&
     other.user == user &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (permissions == null ? 0 : permissions.hashCode) +
    (userId == null ? 0 : userId.hashCode) +
    (user == null ? 0 : user.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'AdminUser[id=$id, permissions=$permissions, userId=$userId, user=$user, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (permissions != null) {
      json[r'permissions'] = permissions;
    }
    if (userId != null) {
      json[r'userId'] = userId;
    }
    if (user != null) {
      json[r'user'] = user;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [AdminUser] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static AdminUser fromJson(Map<String, dynamic> json) => json == null
    ? null
    : AdminUser(
        id: json[r'id'],
        permissions: Permissions.fromJson(json[r'permissions']),
        userId: json[r'userId'],
        user: User.fromJson(json[r'user']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<AdminUser> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <AdminUser>[]
      : json.map((v) => AdminUser.fromJson(v)).toList(growable: true == growable);

  static Map<String, AdminUser> mapFromJson(Map<String, dynamic> json) {
    final map = <String, AdminUser>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = AdminUser.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of AdminUser-objects as value to a dart map
  static Map<String, List<AdminUser>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<AdminUser>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = AdminUser.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

