//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CartCampaignRule {
  /// Returns a new [CartCampaignRule] instance.
  CartCampaignRule({
    this.entityId,
    this.ruleType,
    this.operation,
    this.checkAgainst,
  });

  int entityId;

  CartCampaignRuleType ruleType;

  CartCampaignOperation operation;

  double checkAgainst;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CartCampaignRule &&
     other.entityId == entityId &&
     other.ruleType == ruleType &&
     other.operation == operation &&
     other.checkAgainst == checkAgainst;

  @override
  int get hashCode =>
    (entityId == null ? 0 : entityId.hashCode) +
    (ruleType == null ? 0 : ruleType.hashCode) +
    (operation == null ? 0 : operation.hashCode) +
    (checkAgainst == null ? 0 : checkAgainst.hashCode);

  @override
  String toString() => 'CartCampaignRule[entityId=$entityId, ruleType=$ruleType, operation=$operation, checkAgainst=$checkAgainst]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (entityId != null) {
      json[r'entityId'] = entityId;
    }
    if (ruleType != null) {
      json[r'ruleType'] = ruleType;
    }
    if (operation != null) {
      json[r'operation'] = operation;
    }
    if (checkAgainst != null) {
      json[r'checkAgainst'] = checkAgainst;
    }
    return json;
  }

  /// Returns a new [CartCampaignRule] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CartCampaignRule fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CartCampaignRule(
        entityId: json[r'entityId'],
        ruleType: CartCampaignRuleType.fromJson(json[r'ruleType']),
        operation: CartCampaignOperation.fromJson(json[r'operation']),
        checkAgainst: json[r'checkAgainst'],
    );

  static List<CartCampaignRule> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CartCampaignRule>[]
      : json.map((v) => CartCampaignRule.fromJson(v)).toList(growable: true == growable);

  static Map<String, CartCampaignRule> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CartCampaignRule>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CartCampaignRule.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CartCampaignRule-objects as value to a dart map
  static Map<String, List<CartCampaignRule>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CartCampaignRule>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CartCampaignRule.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

