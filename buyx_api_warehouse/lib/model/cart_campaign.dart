//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CartCampaign {
  /// Returns a new [CartCampaign] instance.
  CartCampaign({
    this.id,
    this.name,
    this.discountType,
    this.discountEffect,
    this.discountAmount,
    this.totalUsed,
    this.maxUseCount,
    this.enabled,
    this.cartCampaignRules,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String name;

  DiscountType discountType;

  DiscountEffect discountEffect;

  double discountAmount;

  int totalUsed;

  int maxUseCount;

  bool enabled;

  List<CartCampaignRule> cartCampaignRules;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CartCampaign &&
     other.id == id &&
     other.name == name &&
     other.discountType == discountType &&
     other.discountEffect == discountEffect &&
     other.discountAmount == discountAmount &&
     other.totalUsed == totalUsed &&
     other.maxUseCount == maxUseCount &&
     other.enabled == enabled &&
     other.cartCampaignRules == cartCampaignRules &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (discountType == null ? 0 : discountType.hashCode) +
    (discountEffect == null ? 0 : discountEffect.hashCode) +
    (discountAmount == null ? 0 : discountAmount.hashCode) +
    (totalUsed == null ? 0 : totalUsed.hashCode) +
    (maxUseCount == null ? 0 : maxUseCount.hashCode) +
    (enabled == null ? 0 : enabled.hashCode) +
    (cartCampaignRules == null ? 0 : cartCampaignRules.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'CartCampaign[id=$id, name=$name, discountType=$discountType, discountEffect=$discountEffect, discountAmount=$discountAmount, totalUsed=$totalUsed, maxUseCount=$maxUseCount, enabled=$enabled, cartCampaignRules=$cartCampaignRules, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (discountType != null) {
      json[r'discountType'] = discountType;
    }
    if (discountEffect != null) {
      json[r'discountEffect'] = discountEffect;
    }
    if (discountAmount != null) {
      json[r'discountAmount'] = discountAmount;
    }
    if (totalUsed != null) {
      json[r'totalUsed'] = totalUsed;
    }
    if (maxUseCount != null) {
      json[r'maxUseCount'] = maxUseCount;
    }
    if (enabled != null) {
      json[r'enabled'] = enabled;
    }
    if (cartCampaignRules != null) {
      json[r'cartCampaignRules'] = cartCampaignRules;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [CartCampaign] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CartCampaign fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CartCampaign(
        id: json[r'id'],
        name: json[r'name'],
        discountType: DiscountType.fromJson(json[r'discountType']),
        discountEffect: DiscountEffect.fromJson(json[r'discountEffect']),
        discountAmount: json[r'discountAmount'],
        totalUsed: json[r'totalUsed'],
        maxUseCount: json[r'maxUseCount'],
        enabled: json[r'enabled'],
        cartCampaignRules: CartCampaignRule.listFromJson(json[r'cartCampaignRules']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<CartCampaign> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CartCampaign>[]
      : json.map((v) => CartCampaign.fromJson(v)).toList(growable: true == growable);

  static Map<String, CartCampaign> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CartCampaign>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CartCampaign.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CartCampaign-objects as value to a dart map
  static Map<String, List<CartCampaign>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CartCampaign>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CartCampaign.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

