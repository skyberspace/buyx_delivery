//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CoordinateSequence {
  /// Returns a new [CoordinateSequence] instance.
  CoordinateSequence({
    this.dimension,
    this.measures,
    this.spatial,
    this.ordinates,
    this.hasZ,
    this.hasM,
    this.zOrdinateIndex,
    this.mOrdinateIndex,
    this.count,
  });

  int dimension;

  int measures;

  int spatial;

  Ordinates ordinates;

  bool hasZ;

  bool hasM;

  int zOrdinateIndex;

  int mOrdinateIndex;

  int count;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CoordinateSequence &&
     other.dimension == dimension &&
     other.measures == measures &&
     other.spatial == spatial &&
     other.ordinates == ordinates &&
     other.hasZ == hasZ &&
     other.hasM == hasM &&
     other.zOrdinateIndex == zOrdinateIndex &&
     other.mOrdinateIndex == mOrdinateIndex &&
     other.count == count;

  @override
  int get hashCode =>
    (dimension == null ? 0 : dimension.hashCode) +
    (measures == null ? 0 : measures.hashCode) +
    (spatial == null ? 0 : spatial.hashCode) +
    (ordinates == null ? 0 : ordinates.hashCode) +
    (hasZ == null ? 0 : hasZ.hashCode) +
    (hasM == null ? 0 : hasM.hashCode) +
    (zOrdinateIndex == null ? 0 : zOrdinateIndex.hashCode) +
    (mOrdinateIndex == null ? 0 : mOrdinateIndex.hashCode) +
    (count == null ? 0 : count.hashCode);

  @override
  String toString() => 'CoordinateSequence[dimension=$dimension, measures=$measures, spatial=$spatial, ordinates=$ordinates, hasZ=$hasZ, hasM=$hasM, zOrdinateIndex=$zOrdinateIndex, mOrdinateIndex=$mOrdinateIndex, count=$count]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (dimension != null) {
      json[r'dimension'] = dimension;
    }
    if (measures != null) {
      json[r'measures'] = measures;
    }
    if (spatial != null) {
      json[r'spatial'] = spatial;
    }
    if (ordinates != null) {
      json[r'ordinates'] = ordinates;
    }
    if (hasZ != null) {
      json[r'hasZ'] = hasZ;
    }
    if (hasM != null) {
      json[r'hasM'] = hasM;
    }
    if (zOrdinateIndex != null) {
      json[r'zOrdinateIndex'] = zOrdinateIndex;
    }
    if (mOrdinateIndex != null) {
      json[r'mOrdinateIndex'] = mOrdinateIndex;
    }
    if (count != null) {
      json[r'count'] = count;
    }
    return json;
  }

  /// Returns a new [CoordinateSequence] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CoordinateSequence fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CoordinateSequence(
        dimension: json[r'dimension'],
        measures: json[r'measures'],
        spatial: json[r'spatial'],
        ordinates: Ordinates.fromJson(json[r'ordinates']),
        hasZ: json[r'hasZ'],
        hasM: json[r'hasM'],
        zOrdinateIndex: json[r'zOrdinateIndex'],
        mOrdinateIndex: json[r'mOrdinateIndex'],
        count: json[r'count'],
    );

  static List<CoordinateSequence> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CoordinateSequence>[]
      : json.map((v) => CoordinateSequence.fromJson(v)).toList(growable: true == growable);

  static Map<String, CoordinateSequence> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CoordinateSequence>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CoordinateSequence.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CoordinateSequence-objects as value to a dart map
  static Map<String, List<CoordinateSequence>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CoordinateSequence>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CoordinateSequence.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

