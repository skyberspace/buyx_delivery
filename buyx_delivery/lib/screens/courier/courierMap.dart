import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:BuyxCourier/screens/courier/deliveryScreen.dart';
import 'package:BuyxCourier/utilities/constants.dart';
import 'package:buyx_api_warehouse/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:BuyxCourier/main.dart';
import 'package:BuyxCourier/models/geolocator.dart';
import 'package:BuyxCourier/ui/widgets/iboxCircle.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:url_launcher/url_launcher.dart';

import '../../api.dart';

// ignore: must_be_immutable
class CourierMap extends StatefulWidget {
  CourierMap({this.orderId});
  int orderId;
  @override
  _CourierMapState createState() => _CourierMapState();
}

class _CourierMapState extends State<CourierMap> {
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  //
  //
  //
  _onOrderDetails() {
    Navigator.pop(context);
  }

  _backButtonPress() {
    Navigator.pop(context);
  }

  //
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  var windowWidth;
  var windowHeight;
  String _currentDistance = "";
  String _mapStyle;
  var location = Location();
  double _currentZoom = 12;
  OrderBR orderDetails;
  //var orderDetails;
  _initCameraPosition(OrderBR orderDetails, LatLng coord) async {
    // calculate zoom
    try {
      LatLng latLng_1 = LatLng(
        /*item.address1Latitude activeOrderElements.address1Latitude*/ orderDetails
            .data.pickupAddress.latitude,
        /*item.address1Longitude*/ orderDetails.data.pickupAddress.longitude,
      );
      LatLng latLng_2 = LatLng(orderDetails.data.orderAddress.latitude,
          orderDetails.data.orderAddress.longitude);
      dprint("latLng_1 = $latLng_1");
      dprint("latLng_2 = $latLng_2");
      //    LatLngBounds bound;
      //    if (latLng_1.latitude >= latLng_2.latitude) {
      //      if (latLng_1.longitude <= latLng_2.longitude){
      //        latLng_1 = LatLng(item.address1Latitude, item.address2Longitude,);
      //        latLng_2 = LatLng(item.address2Latitude, item.address1Longitude);
      //      }
      //      bound = LatLngBounds(southwest: latLng_2, northeast: latLng_1);
      //    }else
      //      bound = LatLngBounds(southwest: latLng_1, northeast: latLng_2); // юго-запад - северо-восток

      var lat1 = latLng_1.latitude; // широта
      var lat2 = latLng_2.latitude;
      if (latLng_1.latitude > latLng_2.latitude) {
        lat1 = latLng_2.latitude;
        lat2 = latLng_1.latitude;
      }
      var lng1 = latLng_1.longitude;
      var lng2 = latLng_2.longitude;
      if (latLng_1.longitude > latLng_2.longitude) {
        lng1 = latLng_2.longitude;
        lng2 = latLng_1.longitude;
      }
      dprint("lat1 = $lat1, lat2 = $lat2");
      dprint("lng1 = $lng1, lng2 = $lng2");
      LatLngBounds bound = LatLngBounds(
          southwest: LatLng(lat1, lng1),
          northeast: LatLng(lat2, lng2)); // юго-запад - северо-восток
      dprint(bound.toString());

      CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);
      if (_controller != null)
        _controller.animateCamera(u2).then((void v) {
          // check(u2,_controller);
        });

      double distance = await location.distanceBetween(
          orderDetails.data.pickupAddress.latitude,
          orderDetails.data.pickupAddress.longitude,
          orderDetails.data.orderAddress.latitude,
          orderDetails.data.orderAddress.longitude);

      if (distance < 20000) {
        _currentDistance = (distance / 1000).toStringAsFixed(3);
      } // to km
      else {
        _currentDistance = (distance / 1000).toStringAsFixed(0); // to km
      }
      setState(() {});
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  Map<PolylineId, Polyline> _mapPolylines = {};
  int _polylineIdCounter = 1;
  Future<void> _add() async {
    try {
      if (/*item.id == account.openOrderOnMap*/ true) {
        final String polylineIdVal = 'polyline_id_$_polylineIdCounter';
        _polylineIdCounter++;
        final PolylineId polylineId = PolylineId(polylineIdVal);

        var polylinePoints = PolylinePoints();
        PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
          /*appSettings.googleApi*/ "AIzaSyDZZ95Gu67iSeQWI_yNKkl9uiiHj-dytac",
          PointLatLng(orderDetails.data.pickupAddress.latitude,
              orderDetails.data.pickupAddress.longitude),
          PointLatLng(orderDetails.data.orderAddress.latitude,
              orderDetails.data.orderAddress.longitude),
          travelMode: TravelMode.driving,
        );
        List<LatLng> polylineCoordinates = [];

        if (result.points.isNotEmpty) {
          result.points.forEach((PointLatLng point) {
            polylineCoordinates.add(LatLng(point.latitude, point.longitude));
          });
        }

        final Polyline polyline = Polyline(
            polylineId: polylineId,
            consumeTapEvents: true,
            color: Colors.red,
            width: 4,
            points: polylineCoordinates,
            visible: true);

        setState(() {
          _mapPolylines[polylineId] = polyline;
        });
        LatLng coordinates = LatLng(
          orderDetails.data.pickupAddress.latitude,
          orderDetails.data.pickupAddress.longitude,
        );
        if (polylineCoordinates.isNotEmpty)
          coordinates = polylineCoordinates[polylineCoordinates.length ~/ 2];
        _initCameraPosition(orderDetails, coordinates);
        _addMarker(orderDetails);
      }
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _callUser() async {
    try {
      if (/*activeOrderElements.id == account.openOrderOnMap*/ true) {
        var uri = 'tel:${orderDetails.data.user.phone}';
        if (await canLaunch(uri)) await launch(uri);
      }
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _load() async {
    try {
      await _successCheck();
      await _add();
      _kGooglePlex = CameraPosition(
        target: LatLng(orderDetails.data.pickupAddress.latitude,
            orderDetails.data.pickupAddress.longitude),
        zoom: _currentZoom,
      ); // paris coordinates

      rootBundle.loadString('assets/map_style.txt').then((string) {
        _mapStyle = string;
      });
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  @override
  void initState() {
    _load();
    _initIcons();
    super.initState();
  }

  _initIcons() async {
    try {
      final Uint8List markerIcon =
          await getBytesFromAsset('assets/marker1.png', 150);
      _iconHome = BitmapDescriptor.fromBytes(markerIcon);
      final Uint8List markerIcon2 =
          await getBytesFromAsset('assets/marker2.png', 150);
      _iconDest = BitmapDescriptor.fromBytes(markerIcon2);
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    try {
      ByteData data = await rootBundle.load(path);
      ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
          targetWidth: width);
      ui.FrameInfo fi = await codec.getNextFrame();
      return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
          .buffer
          .asUint8List();
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(48.858093, 2.294694),
    zoom: 12,
  ); // paris eifell coordinates
  Set<Marker> markers = {};
  GoogleMapController _controller;

  @override
  Widget build(BuildContext context) {
    windowWidth = MediaQuery.of(context).size.width;
    windowHeight = MediaQuery.of(context).size.height;
    if (_controller != null) if (theme.darkMode)
      _controller.setMapStyle(_mapStyle);
    else
      _controller.setMapStyle(null);

    return WillPopScope(
        onWillPop: () async {
          _backButtonPress();
          return false;
        },
        child: Scaffold(
          body: Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: Stack(children: <Widget>[
                _map(),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 15,
                          ),
                          _buttonPlus(),
                          _buttonMinus(),
                          _buttonMyLocation(),
                        ],
                      )),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Column(
                        children: <Widget>[
                          SizedBox(
                            height: 15,
                          ),
                          _buttonBack(),
                          _buttonCall(),
                          _buttonOrder(),
                        ],
                      )),
                ),
                if (_currentDistance.isNotEmpty)
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                        margin: EdgeInsets.only(top: 10),
                        child: Container(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 10, bottom: 10),
                            decoration: BoxDecoration(
                              color: theme.colorBackgroundDialog,
                              borderRadius: new BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withAlpha(40),
                                  spreadRadius: 6,
                                  blurRadius: 6,
                                  offset: Offset(
                                      2, 2), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Text(
                              "Distance : $_currentDistance km" /*${appSettings.distanceUnit}"*/,
                              style: TextStyle(
                                  fontFamily: kNormalSizeText.fontFamily,
                                  fontSize: 22,
                                  fontWeight: FontWeight.bold),
                            ) // Distance 4 km
                            )),
                  ),
              ])),
        ));
  }

  _successCheck() async {
    try {
      orderDetails = await Api.instance.orderPreparing
          .orderPreparingGetOrderFromIdPost(
              orderIdRequest: OrderIdRequest(orderId: widget.orderId));
      if (!orderDetails.success) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Warning!",
                style: TextStyle(color: Colors.redAccent),
              ),
              content: Text(
                /*"Connection lost with server. Please check internet settings and try again. For further information please contact buyx operation team."*/ orderDetails
                    .errors[0],
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "OK",
                  ),
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DeliveryScreen(),
                      ),
                    );
                  },
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _map() {
    try {
      return GoogleMap(
          mapType: MapType.normal,
          zoomGesturesEnabled: true,
          zoomControlsEnabled:
              false, // Whether to show zoom controls (only applicable for Android).
          myLocationEnabled:
              true, // For showing your current location on the map with a blue dot.
          myLocationButtonEnabled:
              false, // This button is used to bring the user location to the center of the camera view.
          initialCameraPosition: _kGooglePlex,
          polylines: Set<Polyline>.of(_mapPolylines.values),
          onCameraMove: (CameraPosition cameraPosition) {
            _currentZoom = cameraPosition.zoom;
          },
          onTap: (LatLng pos) {},
          onLongPress: (LatLng pos) {},
          markers: markers != null ? Set<Marker>.from(markers) : null,
          onMapCreated: (GoogleMapController controller) {
            _controller = controller;
            if (theme.darkMode) _controller.setMapStyle(_mapStyle);
          });
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _buttonPlus() {
    try {
      return Stack(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            child: IBoxCircle(
                child: Icon(
              Icons.add,
              size: 30,
              color: Colors.black.withOpacity(0.5),
            )),
          ),
          Container(
            height: 60,
            width: 60,
            child: Material(
                color: Colors.transparent,
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  splashColor: Colors.grey[400],
                  onTap: () {
                    _controller.animateCamera(
                      CameraUpdate.zoomIn(),
                    );
                  }, // needed
                )),
          )
        ],
      );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _buttonMinus() {
    try {
      return Stack(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            child: IBoxCircle(
                child: Icon(
              Icons.remove,
              size: 30,
              color: Colors.black.withOpacity(0.5),
            )),
          ),
          Container(
            height: 60,
            width: 60,
            child: Material(
                color: Colors.transparent,
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  splashColor: Colors.grey[400],
                  onTap: () {
                    _controller.animateCamera(
                      CameraUpdate.zoomOut(),
                    );
                  }, // needed
                )),
          )
        ],
      );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _buttonCall() {
    try {
      return Stack(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            child: IBoxCircle(
                child: Icon(
              Icons.phone,
              size: 30,
              color: Colors.black.withOpacity(0.5),
            )),
          ),
          Container(
            height: 60,
            width: 60,
            child: Material(
                color: Colors.transparent,
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  splashColor: Colors.grey[400],
                  onTap: () {
                    _callUser();
                  }, // needed
                )),
          )
        ],
      );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _buttonOrder() {
    try {
      return Stack(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            child: IBoxCircle(
                child: Icon(
              Icons.assignment,
              size: 30,
              color: Colors.black.withOpacity(0.5),
            )),
          ),
          Container(
            height: 60,
            width: 60,
            child: Material(
                color: Colors.transparent,
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  splashColor: Colors.grey[400],
                  onTap: () {
                    _onOrderDetails();
                  }, // needed
                )),
          )
        ],
      );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _buttonMyLocation() {
    try {
      return Stack(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            child: IBoxCircle(
              child: Icon(
                Icons.my_location,
                size: 30,
                color: Colors.black.withOpacity(0.5),
              ),
            ),
          ),
          Container(
            height: 60,
            width: 60,
            child: Material(
                color: Colors.transparent,
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  splashColor: Colors.grey[400],
                  onTap: () {
                    _getCurrentLocation();
                  }, // needed
                )),
          )
        ],
      );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _getCurrentLocation() async {
    try {
      var position = await location.getCurrent();
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(position.latitude, position.longitude),
            zoom: _currentZoom,
          ),
        ),
      );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  MarkerId _lastMarkerId;

  var _iconHome;
  var _iconDest;

  _addMarker(OrderBR orderDetails) {
    try {
      print("add marker ${orderDetails.data.id}");
      _lastMarkerId = MarkerId("addr1${orderDetails.data.id}");
      final marker = Marker(
          markerId: _lastMarkerId,
          icon: _iconHome,
          position: LatLng(
            orderDetails.data.pickupAddress.latitude,
            orderDetails.data.pickupAddress.longitude,
          ),
          onTap: () {});
      markers.add(marker);
      _lastMarkerId = MarkerId("addr2${orderDetails.data.id}");
      final marker2 = Marker(
          markerId: _lastMarkerId,
          icon: _iconDest,
          position: LatLng(
            orderDetails.data.orderAddress.latitude,
            orderDetails.data.orderAddress.longitude,
          ),
          onTap: () {});
      markers.add(marker2);
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  _buttonBack() {
    try {
      return Stack(
        children: <Widget>[
          Container(
            height: 60,
            width: 60,
            child: IBoxCircle(
                child: Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: Colors.black.withOpacity(0.5),
                    ))),
          ),
          Container(
            height: 60,
            width: 60,
            child: Material(
                color: Colors.transparent,
                shape: CircleBorder(),
                clipBehavior: Clip.hardEdge,
                child: InkWell(
                  splashColor: Colors.grey[400],
                  onTap: () {
                    _backButtonPress();
                  }, // needed
                )),
          )
        ],
      );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }
}
