//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_warehouse/api.dart';
import 'package:test/test.dart';


/// tests for WarehouseOrderPreparingApi
void main() {
  final instance = WarehouseOrderPreparingApi();

  group('tests for WarehouseOrderPreparingApi', () {
    // assign this packages delivery job to current user
    //
    //Future<OrderBR> orderPreparingAssignDeliveryToMePost({ AssignRequest assignRequest }) async
    test('test orderPreparingAssignDeliveryToMePost', () async {
      // TODO
    });

    // assign given delivery job to an user
    //
    //Future<OrderBR> orderPreparingAssignDeliveryToUserPost({ AssignToOtherRequest assignToOtherRequest }) async
    test('test orderPreparingAssignDeliveryToUserPost', () async {
      // TODO
    });

    // assign the pakcaging job to current user
    //
    //Future<OrderBR> orderPreparingAssignPackagingToMePost({ AssignRequest assignRequest }) async
    test('test orderPreparingAssignPackagingToMePost', () async {
      // TODO
    });

    // assaign the packagint to an user  only supervisor accoutns can call this
    //
    //Future<OrderBR> orderPreparingAssignPackagingToUserPost({ AssignToOtherRequest assignToOtherRequest }) async
    test('test orderPreparingAssignPackagingToUserPost', () async {
      // TODO
    });

    // Returns the order if the user is assigned to any order right now  Returns null if the user is unoccupied  Call this when starting the delivery app to see if the user has any packages assigned to them  If they do redirect them accordingly
    //
    //Future<OrderBR> orderPreparingGetCurrentOrderPost() async
    test('test orderPreparingGetCurrentOrderPost', () async {
      // TODO
    });

    // Returns delivery history for the current user
    //
    //Future<OrderListBR> orderPreparingGetDeliveryHistoryPost() async
    test('test orderPreparingGetDeliveryHistoryPost', () async {
      // TODO
    });

    // Get order from Id
    //
    //Future<OrderBR> orderPreparingGetOrderFromIdPost({ OrderIdRequest orderIdRequest }) async
    test('test orderPreparingGetOrderFromIdPost', () async {
      // TODO
    });

    // Returns orders ready for delivery for the current user
    //
    //Future<OrderListBR> orderPreparingGetOrdersForDeliveryPost() async
    test('test orderPreparingGetOrdersForDeliveryPost', () async {
      // TODO
    });

    // returns orders that needs to be packaged for the current user
    //
    //Future<OrderListBR> orderPreparingGetOrdersForPreparingPost() async
    test('test orderPreparingGetOrdersForPreparingPost', () async {
      // TODO
    });

    // Returns package history for the current user
    //
    //Future<OrderListBR> orderPreparingGetPackagingHistoryPost() async
    test('test orderPreparingGetPackagingHistoryPost', () async {
      // TODO
    });

    // set the order as delivered
    //
    //Future<BR> orderPreparingOrderDeliveredPost({ AssignRequest assignRequest }) async
    test('test orderPreparingOrderDeliveredPost', () async {
      // TODO
    });

    // sets the order as ready for delivery
    //
    //Future<BR> orderPreparingReadyForDeliveryPost({ AssignRequest assignRequest }) async
    test('test orderPreparingReadyForDeliveryPost', () async {
      // TODO
    });

    // Sets last known delivery location for order
    //
    //Future<BR> orderPreparingSetOrderDeliveryLastLocationPost({ OrderDeliveryLocationRequest orderDeliveryLocationRequest }) async
    test('test orderPreparingSetOrderDeliveryLastLocationPost', () async {
      // TODO
    });

  });
}
