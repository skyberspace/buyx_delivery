//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AssignRequest {
  /// Returns a new [AssignRequest] instance.
  AssignRequest({
    this.orderId,
  });

  int orderId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AssignRequest &&
     other.orderId == orderId;

  @override
  int get hashCode =>
    (orderId == null ? 0 : orderId.hashCode);

  @override
  String toString() => 'AssignRequest[orderId=$orderId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (orderId != null) {
      json[r'orderId'] = orderId;
    }
    return json;
  }

  /// Returns a new [AssignRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static AssignRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : AssignRequest(
        orderId: json[r'orderId'],
    );

  static List<AssignRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <AssignRequest>[]
      : json.map((v) => AssignRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, AssignRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, AssignRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = AssignRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of AssignRequest-objects as value to a dart map
  static Map<String, List<AssignRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<AssignRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = AssignRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

