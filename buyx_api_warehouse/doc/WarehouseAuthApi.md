# buyx_api_warehouse.api.WarehouseAuthApi

## Load the API package
```dart
import 'package:buyx_api_warehouse/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**warehouseAuthGetOTPLoginStatusPost**](WarehouseAuthApi.md#warehouseauthgetotploginstatuspost) | **POST** /warehouse/auth/GetOTPLoginStatus | returns true if OTP login is enabled for warehouse logins  if enabled LoginWithOTP -> LoginWithOTPStep2 methods should be used to login  if not Login method can be used
[**warehouseAuthGetUserFromTokenPost**](WarehouseAuthApi.md#warehouseauthgetuserfromtokenpost) | **POST** /warehouse/auth/GetUserFromToken | 
[**warehouseAuthLoginPost**](WarehouseAuthApi.md#warehouseauthloginpost) | **POST** /warehouse/auth/Login | 
[**warehouseAuthLoginWithOTPPost**](WarehouseAuthApi.md#warehouseauthloginwithotppost) | **POST** /warehouse/auth/LoginWithOTP | Login and get a otp code for 2fa  returned string is the OTP request code and must be provided to LoginWithOTPStep2 action with 6 digit OTP for login
[**warehouseAuthLoginWithOTPStep2Post**](WarehouseAuthApi.md#warehouseauthloginwithotpstep2post) | **POST** /warehouse/auth/LoginWithOTPStep2 | OTP Login check otp code
[**warehouseAuthLogoutPost**](WarehouseAuthApi.md#warehouseauthlogoutpost) | **POST** /warehouse/auth/Logout | 


# **warehouseAuthGetOTPLoginStatusPost**
> BooleanBR warehouseAuthGetOTPLoginStatusPost()

returns true if OTP login is enabled for warehouse logins  if enabled LoginWithOTP -> LoginWithOTPStep2 methods should be used to login  if not Login method can be used

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';

final api_instance = WarehouseAuthApi();

try { 
    final result = api_instance.warehouseAuthGetOTPLoginStatusPost();
    print(result);
} catch (e) {
    print('Exception when calling WarehouseAuthApi->warehouseAuthGetOTPLoginStatusPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**BooleanBR**](BooleanBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **warehouseAuthGetUserFromTokenPost**
> AuthTokenBR warehouseAuthGetUserFromTokenPost()



### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';

final api_instance = WarehouseAuthApi();

try { 
    final result = api_instance.warehouseAuthGetUserFromTokenPost();
    print(result);
} catch (e) {
    print('Exception when calling WarehouseAuthApi->warehouseAuthGetUserFromTokenPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthTokenBR**](AuthTokenBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **warehouseAuthLoginPost**
> AuthTokenBR warehouseAuthLoginPost(phone, password)



### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';

final api_instance = WarehouseAuthApi();
final phone = phone_example; // String | 
final password = password_example; // String | 

try { 
    final result = api_instance.warehouseAuthLoginPost(phone, password);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseAuthApi->warehouseAuthLoginPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **String**|  | [optional] 
 **password** | **String**|  | [optional] 

### Return type

[**AuthTokenBR**](AuthTokenBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **warehouseAuthLoginWithOTPPost**
> StringBR warehouseAuthLoginWithOTPPost(phone, password)

Login and get a otp code for 2fa  returned string is the OTP request code and must be provided to LoginWithOTPStep2 action with 6 digit OTP for login

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';

final api_instance = WarehouseAuthApi();
final phone = phone_example; // String | 
final password = password_example; // String | 

try { 
    final result = api_instance.warehouseAuthLoginWithOTPPost(phone, password);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseAuthApi->warehouseAuthLoginWithOTPPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone** | **String**|  | [optional] 
 **password** | **String**|  | [optional] 

### Return type

[**StringBR**](StringBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **warehouseAuthLoginWithOTPStep2Post**
> AuthTokenBR warehouseAuthLoginWithOTPStep2Post(code, OTP)

OTP Login check otp code

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';

final api_instance = WarehouseAuthApi();
final code = code_example; // String | 
final OTP = OTP_example; // String | 

try { 
    final result = api_instance.warehouseAuthLoginWithOTPStep2Post(code, OTP);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseAuthApi->warehouseAuthLoginWithOTPStep2Post: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**|  | [optional] 
 **OTP** | **String**|  | [optional] 

### Return type

[**AuthTokenBR**](AuthTokenBR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **warehouseAuthLogoutPost**
> BR warehouseAuthLogoutPost(token)



### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';

final api_instance = WarehouseAuthApi();
final token = token_example; // String | 

try { 
    final result = api_instance.warehouseAuthLogoutPost(token);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseAuthApi->warehouseAuthLogoutPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

