//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AuthToken {
  /// Returns a new [AuthToken] instance.
  AuthToken({
    this.id,
    this.token,
    this.expires,
    this.type,
    this.userId,
    this.user,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String token;

  DateTime expires;

  String type;

  int userId;

  User user;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AuthToken &&
     other.id == id &&
     other.token == token &&
     other.expires == expires &&
     other.type == type &&
     other.userId == userId &&
     other.user == user &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (token == null ? 0 : token.hashCode) +
    (expires == null ? 0 : expires.hashCode) +
    (type == null ? 0 : type.hashCode) +
    (userId == null ? 0 : userId.hashCode) +
    (user == null ? 0 : user.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'AuthToken[id=$id, token=$token, expires=$expires, type=$type, userId=$userId, user=$user, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (token != null) {
      json[r'token'] = token;
    }
    if (expires != null) {
      json[r'expires'] = expires.toUtc().toIso8601String();
    }
    if (type != null) {
      json[r'type'] = type;
    }
    if (userId != null) {
      json[r'userId'] = userId;
    }
    if (user != null) {
      json[r'user'] = user;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [AuthToken] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static AuthToken fromJson(Map<String, dynamic> json) => json == null
    ? null
    : AuthToken(
        id: json[r'id'],
        token: json[r'token'],
        expires: json[r'expires'] == null
          ? null
          : DateTime.parse(json[r'expires']),
        type: json[r'type'],
        userId: json[r'userId'],
        user: User.fromJson(json[r'user']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<AuthToken> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <AuthToken>[]
      : json.map((v) => AuthToken.fromJson(v)).toList(growable: true == growable);

  static Map<String, AuthToken> mapFromJson(Map<String, dynamic> json) {
    final map = <String, AuthToken>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = AuthToken.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of AuthToken-objects as value to a dart map
  static Map<String, List<AuthToken>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<AuthToken>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = AuthToken.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

