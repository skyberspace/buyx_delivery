//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class UserFavorite {
  /// Returns a new [UserFavorite] instance.
  UserFavorite({
    this.id,
    this.productId,
    this.userId,
    this.product,
    this.user,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  int productId;

  int userId;

  Product product;

  User user;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is UserFavorite &&
     other.id == id &&
     other.productId == productId &&
     other.userId == userId &&
     other.product == product &&
     other.user == user &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (productId == null ? 0 : productId.hashCode) +
    (userId == null ? 0 : userId.hashCode) +
    (product == null ? 0 : product.hashCode) +
    (user == null ? 0 : user.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'UserFavorite[id=$id, productId=$productId, userId=$userId, product=$product, user=$user, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (userId != null) {
      json[r'userId'] = userId;
    }
    if (product != null) {
      json[r'product'] = product;
    }
    if (user != null) {
      json[r'user'] = user;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [UserFavorite] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static UserFavorite fromJson(Map<String, dynamic> json) => json == null
    ? null
    : UserFavorite(
        id: json[r'id'],
        productId: json[r'productId'],
        userId: json[r'userId'],
        product: Product.fromJson(json[r'product']),
        user: User.fromJson(json[r'user']),
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<UserFavorite> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <UserFavorite>[]
      : json.map((v) => UserFavorite.fromJson(v)).toList(growable: true == growable);

  static Map<String, UserFavorite> mapFromJson(Map<String, dynamic> json) {
    final map = <String, UserFavorite>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = UserFavorite.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of UserFavorite-objects as value to a dart map
  static Map<String, List<UserFavorite>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<UserFavorite>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = UserFavorite.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

