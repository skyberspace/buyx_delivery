# buyx_api_warehouse.api.WarehouseOrderPreparingApi

## Load the API package
```dart
import 'package:buyx_api_warehouse/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**orderPreparingAssignDeliveryToMePost**](WarehouseOrderPreparingApi.md#orderpreparingassigndeliverytomepost) | **POST** /orderPreparing/AssignDeliveryToMe | assign this packages delivery job to current user
[**orderPreparingAssignDeliveryToUserPost**](WarehouseOrderPreparingApi.md#orderpreparingassigndeliverytouserpost) | **POST** /orderPreparing/AssignDeliveryToUser | assign given delivery job to an user
[**orderPreparingAssignPackagingToMePost**](WarehouseOrderPreparingApi.md#orderpreparingassignpackagingtomepost) | **POST** /orderPreparing/AssignPackagingToMe | assign the pakcaging job to current user
[**orderPreparingAssignPackagingToUserPost**](WarehouseOrderPreparingApi.md#orderpreparingassignpackagingtouserpost) | **POST** /orderPreparing/AssignPackagingToUser | assaign the packagint to an user  only supervisor accoutns can call this
[**orderPreparingGetCurrentOrderPost**](WarehouseOrderPreparingApi.md#orderpreparinggetcurrentorderpost) | **POST** /orderPreparing/GetCurrentOrder | Returns the order if the user is assigned to any order right now  Returns null if the user is unoccupied  Call this when starting the delivery app to see if the user has any packages assigned to them  If they do redirect them accordingly
[**orderPreparingGetDeliveryHistoryPost**](WarehouseOrderPreparingApi.md#orderpreparinggetdeliveryhistorypost) | **POST** /orderPreparing/GetDeliveryHistory | Returns delivery history for the current user
[**orderPreparingGetOrderFromIdPost**](WarehouseOrderPreparingApi.md#orderpreparinggetorderfromidpost) | **POST** /orderPreparing/GetOrderFromId | Get order from Id
[**orderPreparingGetOrdersForDeliveryPost**](WarehouseOrderPreparingApi.md#orderpreparinggetordersfordeliverypost) | **POST** /orderPreparing/GetOrdersForDelivery | Returns orders ready for delivery for the current user
[**orderPreparingGetOrdersForPreparingPost**](WarehouseOrderPreparingApi.md#orderpreparinggetordersforpreparingpost) | **POST** /orderPreparing/GetOrdersForPreparing | returns orders that needs to be packaged for the current user
[**orderPreparingGetPackagingHistoryPost**](WarehouseOrderPreparingApi.md#orderpreparinggetpackaginghistorypost) | **POST** /orderPreparing/GetPackagingHistory | Returns package history for the current user
[**orderPreparingOrderDeliveredPost**](WarehouseOrderPreparingApi.md#orderpreparingorderdeliveredpost) | **POST** /orderPreparing/OrderDelivered | set the order as delivered
[**orderPreparingReadyForDeliveryPost**](WarehouseOrderPreparingApi.md#orderpreparingreadyfordeliverypost) | **POST** /orderPreparing/ReadyForDelivery | sets the order as ready for delivery
[**orderPreparingSetOrderDeliveryLastLocationPost**](WarehouseOrderPreparingApi.md#orderpreparingsetorderdeliverylastlocationpost) | **POST** /orderPreparing/SetOrderDeliveryLastLocation | Sets last known delivery location for order


# **orderPreparingAssignDeliveryToMePost**
> OrderBR orderPreparingAssignDeliveryToMePost(assignRequest)

assign this packages delivery job to current user

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final assignRequest = AssignRequest(); // AssignRequest | 

try { 
    final result = api_instance.orderPreparingAssignDeliveryToMePost(assignRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingAssignDeliveryToMePost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assignRequest** | [**AssignRequest**](AssignRequest.md)|  | [optional] 

### Return type

[**OrderBR**](OrderBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingAssignDeliveryToUserPost**
> OrderBR orderPreparingAssignDeliveryToUserPost(assignToOtherRequest)

assign given delivery job to an user

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final assignToOtherRequest = AssignToOtherRequest(); // AssignToOtherRequest | 

try { 
    final result = api_instance.orderPreparingAssignDeliveryToUserPost(assignToOtherRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingAssignDeliveryToUserPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assignToOtherRequest** | [**AssignToOtherRequest**](AssignToOtherRequest.md)|  | [optional] 

### Return type

[**OrderBR**](OrderBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingAssignPackagingToMePost**
> OrderBR orderPreparingAssignPackagingToMePost(assignRequest)

assign the pakcaging job to current user

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final assignRequest = AssignRequest(); // AssignRequest | 

try { 
    final result = api_instance.orderPreparingAssignPackagingToMePost(assignRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingAssignPackagingToMePost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assignRequest** | [**AssignRequest**](AssignRequest.md)|  | [optional] 

### Return type

[**OrderBR**](OrderBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingAssignPackagingToUserPost**
> OrderBR orderPreparingAssignPackagingToUserPost(assignToOtherRequest)

assaign the packagint to an user  only supervisor accoutns can call this

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final assignToOtherRequest = AssignToOtherRequest(); // AssignToOtherRequest | 

try { 
    final result = api_instance.orderPreparingAssignPackagingToUserPost(assignToOtherRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingAssignPackagingToUserPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assignToOtherRequest** | [**AssignToOtherRequest**](AssignToOtherRequest.md)|  | [optional] 

### Return type

[**OrderBR**](OrderBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingGetCurrentOrderPost**
> OrderBR orderPreparingGetCurrentOrderPost()

Returns the order if the user is assigned to any order right now  Returns null if the user is unoccupied  Call this when starting the delivery app to see if the user has any packages assigned to them  If they do redirect them accordingly

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();

try { 
    final result = api_instance.orderPreparingGetCurrentOrderPost();
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingGetCurrentOrderPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OrderBR**](OrderBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingGetDeliveryHistoryPost**
> OrderListBR orderPreparingGetDeliveryHistoryPost()

Returns delivery history for the current user

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();

try { 
    final result = api_instance.orderPreparingGetDeliveryHistoryPost();
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingGetDeliveryHistoryPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OrderListBR**](OrderListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingGetOrderFromIdPost**
> OrderBR orderPreparingGetOrderFromIdPost(orderIdRequest)

Get order from Id

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final orderIdRequest = OrderIdRequest(); // OrderIdRequest | 

try { 
    final result = api_instance.orderPreparingGetOrderFromIdPost(orderIdRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingGetOrderFromIdPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderIdRequest** | [**OrderIdRequest**](OrderIdRequest.md)|  | [optional] 

### Return type

[**OrderBR**](OrderBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingGetOrdersForDeliveryPost**
> OrderListBR orderPreparingGetOrdersForDeliveryPost()

Returns orders ready for delivery for the current user

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();

try { 
    final result = api_instance.orderPreparingGetOrdersForDeliveryPost();
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingGetOrdersForDeliveryPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OrderListBR**](OrderListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingGetOrdersForPreparingPost**
> OrderListBR orderPreparingGetOrdersForPreparingPost()

returns orders that needs to be packaged for the current user

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();

try { 
    final result = api_instance.orderPreparingGetOrdersForPreparingPost();
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingGetOrdersForPreparingPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OrderListBR**](OrderListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingGetPackagingHistoryPost**
> OrderListBR orderPreparingGetPackagingHistoryPost()

Returns package history for the current user

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();

try { 
    final result = api_instance.orderPreparingGetPackagingHistoryPost();
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingGetPackagingHistoryPost: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**OrderListBR**](OrderListBR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingOrderDeliveredPost**
> BR orderPreparingOrderDeliveredPost(assignRequest)

set the order as delivered

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final assignRequest = AssignRequest(); // AssignRequest | 

try { 
    final result = api_instance.orderPreparingOrderDeliveredPost(assignRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingOrderDeliveredPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assignRequest** | [**AssignRequest**](AssignRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingReadyForDeliveryPost**
> BR orderPreparingReadyForDeliveryPost(assignRequest)

sets the order as ready for delivery

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final assignRequest = AssignRequest(); // AssignRequest | 

try { 
    final result = api_instance.orderPreparingReadyForDeliveryPost(assignRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingReadyForDeliveryPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assignRequest** | [**AssignRequest**](AssignRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **orderPreparingSetOrderDeliveryLastLocationPost**
> BR orderPreparingSetOrderDeliveryLastLocationPost(orderDeliveryLocationRequest)

Sets last known delivery location for order

### Example 
```dart
import 'package:buyx_api_warehouse/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

final api_instance = WarehouseOrderPreparingApi();
final orderDeliveryLocationRequest = OrderDeliveryLocationRequest(); // OrderDeliveryLocationRequest | 

try { 
    final result = api_instance.orderPreparingSetOrderDeliveryLastLocationPost(orderDeliveryLocationRequest);
    print(result);
} catch (e) {
    print('Exception when calling WarehouseOrderPreparingApi->orderPreparingSetOrderDeliveryLastLocationPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orderDeliveryLocationRequest** | [**OrderDeliveryLocationRequest**](OrderDeliveryLocationRequest.md)|  | [optional] 

### Return type

[**BR**](BR.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/_*+json
 - **Accept**: application/json, text/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

