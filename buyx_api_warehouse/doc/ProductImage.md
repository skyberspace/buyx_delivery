# buyx_api_warehouse.model.ProductImage

## Load the model package
```dart
import 'package:buyx_api_warehouse/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**path** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**isPrimary** | **bool** |  | [optional] 
**productId** | **int** |  | [optional] 
**product** | [**Product**](Product.md) |  | [optional] 
**createdAt** | [**DateTime**](DateTime.md) |  | [optional] 
**updatedAt** | [**DateTime**](DateTime.md) |  | [optional] 
**delete** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


