//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class AssignToOtherRequest {
  /// Returns a new [AssignToOtherRequest] instance.
  AssignToOtherRequest({
    this.orderId,
    this.warehouseUserId,
  });

  int orderId;

  int warehouseUserId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is AssignToOtherRequest &&
     other.orderId == orderId &&
     other.warehouseUserId == warehouseUserId;

  @override
  int get hashCode =>
    (orderId == null ? 0 : orderId.hashCode) +
    (warehouseUserId == null ? 0 : warehouseUserId.hashCode);

  @override
  String toString() => 'AssignToOtherRequest[orderId=$orderId, warehouseUserId=$warehouseUserId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (orderId != null) {
      json[r'orderId'] = orderId;
    }
    if (warehouseUserId != null) {
      json[r'warehouseUserId'] = warehouseUserId;
    }
    return json;
  }

  /// Returns a new [AssignToOtherRequest] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static AssignToOtherRequest fromJson(Map<String, dynamic> json) => json == null
    ? null
    : AssignToOtherRequest(
        orderId: json[r'orderId'],
        warehouseUserId: json[r'warehouseUserId'],
    );

  static List<AssignToOtherRequest> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <AssignToOtherRequest>[]
      : json.map((v) => AssignToOtherRequest.fromJson(v)).toList(growable: true == growable);

  static Map<String, AssignToOtherRequest> mapFromJson(Map<String, dynamic> json) {
    final map = <String, AssignToOtherRequest>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = AssignToOtherRequest.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of AssignToOtherRequest-objects as value to a dart map
  static Map<String, List<AssignToOtherRequest>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<AssignToOtherRequest>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = AssignToOtherRequest.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

