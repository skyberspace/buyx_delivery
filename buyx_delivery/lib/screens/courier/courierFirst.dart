import 'package:BuyxCourier/screens/courier/deliveryScreen.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:background_location/background_location.dart';
import 'package:buyx_api_warehouse/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../api.dart';

class CourierFirst extends StatefulWidget {
  @override
  _CourierFirstState createState() => _CourierFirstState();
}

class _CourierFirstState extends State<CourierFirst> {
  List<Order> deliveryOrders;

  bool ready = false;

  OrderListBR orders;

  List<Order> historyDeliveryOrders;

  bool disclosureShown = false;

  Future<void> getOrders(/*String token, success, error*/) async {
    try {
      var orders = await Api.instance.orderPreparing
          .orderPreparingGetOrdersForDeliveryPost();

      deliveryOrders = orders.data;
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  Future<void> getHistoryOrders(/*String token, success, error*/) async {
    try {
      orders = await Api.instance.orderPreparing
          .orderPreparingGetDeliveryHistoryPost();

      historyDeliveryOrders = orders.data;
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  void _activeOrderCheck() async {
    try {
      OrderBR activeOrderCheck =
          await Api.instance.orderPreparing.orderPreparingGetCurrentOrderPost();
      if (activeOrderCheck.success) {
        if (activeOrderCheck.data == null) return null;
        if (activeOrderCheck.data != null) {
          await BackgroundLocation.startLocationService(distanceFilter: 20);
          BackgroundLocation.getLocationUpdates((location) async {
            String latitude = location.latitude.toString();
            String longitude = location.longitude.toString();
            String accuracy = location.accuracy.toString();
            String altitude = location.altitude.toString();
            String bearing = location.bearing.toString();
            await Api.instance.orderPreparing
                .orderPreparingSetOrderDeliveryLastLocationPost(
                    orderDeliveryLocationRequest: OrderDeliveryLocationRequest(
                        orderId: activeOrderCheck.data.id,
                        latitude: location.latitude,
                        longutide: location.longitude));
            print("asd");
          });
          await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return DeliveryScreen(
                orderId: activeOrderCheck.data.id,
              );
            }),
          );
          load();
          return null;
        }
      } else if (!activeOrderCheck.success) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Status!",
                style: TextStyle(color: Colors.redAccent),
              ),
              content: Text(
                activeOrderCheck.errors[0],
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "Try Again",
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    load();
                  },
                ),
              ],
            );
          },
        );
      }
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  showLocationDisclosure({String title, String desc}) {
    if (disclosureShown == false) {
      AwesomeDialog(
        onDissmissCallback: (value) {},
        context: context,
        btnCancel: SizedBox(),
        dialogType: DialogType.NO_HEADER,
        animType: AnimType.BOTTOMSLIDE,
        title: title,
        desc: desc,
        //btnCancelOnPress: () {},
        btnOkOnPress: () {},
      )..show();
      disclosureShown = true;
    }
  }

  @override
  void initState() {
    super.initState();
    load();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      showLocationDisclosure(
          title: 'Background location usage statement',
          desc:
              'This application will use your location to send real-time package location to the buyx customers even when the app is closed or not in use.');
    });
  }

  Future<void> load() async {
    try {
      _activeOrderCheck();
      await getOrders();
      await getHistoryOrders();
      setState(() {
        ready = true;
      });
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black54,
          bottom: TabBar(
            tabs: [
              Tab(
                text: "New",
              ),
              Tab(
                text: "History",
              ),
            ],
          ),
          title: Text('Courier Orders'),
        ),
        body: TabBarView(
          children: [
            if (!ready) ...{
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CupertinoActivityIndicator(
                        animating: true,
                      ),
                      Text("Loading"),
                    ],
                  )
                ],
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CupertinoActivityIndicator(
                        animating: true,
                      ),
                      Text("Loading"),
                    ],
                  )
                ],
              )
            } else ...{
              RefreshIndicator(
                onRefresh: load,
                child: ListView.builder(
                    itemCount: deliveryOrders.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new Card(
                        child: new Container(
                          padding: new EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              new ListTile(
                                leading: Text(
                                    "${deliveryOrders[index].total}" + "£"),
                                //activeColor: Colors.pink[300],
                                dense: true,
                                subtitle: Text(deliveryOrders[index].user.name +
                                    deliveryOrders[index].user.surname),
                                //font change
                                title: new Text(
                                  "${deliveryOrders[index].id}",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5),
                                ),
                                onTap: () async {
                                  await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => DeliveryScreen(
                                        orderId: deliveryOrders[index].id,
                                      ),
                                    ),
                                  );
                                  load();
                                },
                                //value: ListTileModel[index].isCheck,
                                /*secondary: Container(
                                  height: 50,
                                  width: 50,
                                  child: Image.asset(
                                    ListTileModel[index].img,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                onChanged: (bool val) {
                                  //itemChange(val, index);
                                }*/
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              RefreshIndicator(
                onRefresh: load,
                child: ListView.builder(
                    itemCount: historyDeliveryOrders.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new Card(
                        child: new Container(
                          padding: new EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              new ListTile(
                                //activeColor: Colors.pink[300],
                                dense: true,
                                subtitle: Text(historyDeliveryOrders[index]
                                        .user
                                        .name +
                                    historyDeliveryOrders[index].user.surname),
                                //font change
                                title: new Text(
                                  "${historyDeliveryOrders[index].id}",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5),
                                ),
                                leading: Text(
                                    "${historyDeliveryOrders[index].total}£"),
                                onTap: () {
                                  return showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text(
                                          "History Order Detail!",
                                          style: TextStyle(
                                              color: Colors.redAccent),
                                        ),
                                        content: Text(
                                          "The  order ${historyDeliveryOrders[index].id} is delivered at ${DateFormat("dd-MM-yyyy kk:mm").format(historyDeliveryOrders[index].deliveredAt.toLocal())}!",
                                        ),
                                        actions: <Widget>[
                                          new TextButton(
                                            child: new Text(
                                              "OK",
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              ),
            }
          ],
        ),
      ),
    );
  }

  /*void itemChange(bool val, int index) {
    setState(() {
      ListTileModel[index].isCheck = val;
    });
  }*/
  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }
}
