//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

library openapi.api;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:intl/intl.dart';

import 'package:meta/meta.dart';

part 'api_client.dart';
part 'api_helper.dart';
part 'api_exception.dart';
part 'auth/authentication.dart';
part 'auth/api_key_auth.dart';
part 'auth/oauth.dart';
part 'auth/http_basic_auth.dart';
part 'auth/http_bearer_auth.dart';

part 'api/warehouse_auth_api.dart';
part 'api/warehouse_order_preparing_api.dart';
part 'api/warehouse_warehouse_user_api.dart';

part 'model/address.dart';
part 'model/admin_user.dart';
part 'model/assign_request.dart';
part 'model/assign_to_other_request.dart';
part 'model/auth_token.dart';
part 'model/auth_token_br.dart';
part 'model/br.dart';
part 'model/boolean_br.dart';
part 'model/cart_campaign.dart';
part 'model/cart_campaign_operation.dart';
part 'model/cart_campaign_rule.dart';
part 'model/cart_campaign_rule_type.dart';
part 'model/category.dart';
part 'model/coordinate.dart';
part 'model/coordinate_sequence.dart';
part 'model/coordinate_sequence_factory.dart';
part 'model/dimension.dart';
part 'model/discount_effect.dart';
part 'model/discount_type.dart';
part 'model/envelope.dart';
part 'model/geometry.dart';
part 'model/geometry_factory.dart';
part 'model/ogc_geometry_type.dart';
part 'model/order.dart';
part 'model/order_address.dart';
part 'model/order_br.dart';
part 'model/order_delivery_location_request.dart';
part 'model/order_id_request.dart';
part 'model/order_line.dart';
part 'model/order_list_br.dart';
part 'model/order_state.dart';
part 'model/order_state_history.dart';
part 'model/order_type.dart';
part 'model/ordinates.dart';
part 'model/payment_log.dart';
part 'model/permissions.dart';
part 'model/point.dart';
part 'model/precision_model.dart';
part 'model/precision_models.dart';
part 'model/product.dart';
part 'model/product_category.dart';
part 'model/product_image.dart';
part 'model/product_warehouse_stock.dart';
part 'model/status.dart';
part 'model/string_br.dart';
part 'model/submission.dart';
part 'model/user.dart';
part 'model/user_favorite.dart';
part 'model/warehouse.dart';
part 'model/warehouse_role.dart';
part 'model/warehouse_user.dart';
part 'model/web_hook_received_message.dart';


const _delimiters = {'csv': ',', 'ssv': ' ', 'tsv': '\t', 'pipes': '|'};
const _dateEpochMarker = 'epoch';
final _dateFormatter = DateFormat('yyyy-MM-dd');
final _regList = RegExp(r'^List<(.*)>$');
final _regSet = RegExp(r'^Set<(.*)>$');
final _regMap = RegExp(r'^Map<String,(.*)>$');

ApiClient defaultApiClient = ApiClient();
