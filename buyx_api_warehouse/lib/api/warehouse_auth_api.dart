//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class WarehouseAuthApi {
  WarehouseAuthApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// returns true if OTP login is enabled for warehouse logins  if enabled LoginWithOTP -> LoginWithOTPStep2 methods should be used to login  if not Login method can be used
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> warehouseAuthGetOTPLoginStatusPostWithHttpInfo() async {
    final path = r'/warehouse/auth/GetOTPLoginStatus';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// returns true if OTP login is enabled for warehouse logins  if enabled LoginWithOTP -> LoginWithOTPStep2 methods should be used to login  if not Login method can be used
  Future<BooleanBR> warehouseAuthGetOTPLoginStatusPost() async {
    final response = await warehouseAuthGetOTPLoginStatusPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BooleanBR') as BooleanBR;
        }
    return Future<BooleanBR>.value(null);
  }

  /// Performs an HTTP 'POST /warehouse/auth/GetUserFromToken' operation and returns the [Response].
  Future<Response> warehouseAuthGetUserFromTokenPostWithHttpInfo() async {
    final path = r'/warehouse/auth/GetUserFromToken';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  Future<AuthTokenBR> warehouseAuthGetUserFromTokenPost() async {
    final response = await warehouseAuthGetUserFromTokenPostWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AuthTokenBR') as AuthTokenBR;
        }
    return Future<AuthTokenBR>.value(null);
  }

  /// Performs an HTTP 'POST /warehouse/auth/Login' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] phone:
  ///
  /// * [String] password:
  Future<Response> warehouseAuthLoginPostWithHttpInfo({ String phone, String password }) async {
    // Verify required params are set.

    final path = r'/warehouse/auth/Login';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (phone != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'phone', phone));
    }
    if (password != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'password', password));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] phone:
  ///
  /// * [String] password:
  Future<AuthTokenBR> warehouseAuthLoginPost({ String phone, String password }) async {
    final response = await warehouseAuthLoginPostWithHttpInfo( phone: phone, password: password );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AuthTokenBR') as AuthTokenBR;
        }
    return Future<AuthTokenBR>.value(null);
  }

  /// Login and get a otp code for 2fa  returned string is the OTP request code and must be provided to LoginWithOTPStep2 action with 6 digit OTP for login
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] phone:
  ///
  /// * [String] password:
  Future<Response> warehouseAuthLoginWithOTPPostWithHttpInfo({ String phone, String password }) async {
    // Verify required params are set.

    final path = r'/warehouse/auth/LoginWithOTP';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (phone != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'phone', phone));
    }
    if (password != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'password', password));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Login and get a otp code for 2fa  returned string is the OTP request code and must be provided to LoginWithOTPStep2 action with 6 digit OTP for login
  ///
  /// Parameters:
  ///
  /// * [String] phone:
  ///
  /// * [String] password:
  Future<StringBR> warehouseAuthLoginWithOTPPost({ String phone, String password }) async {
    final response = await warehouseAuthLoginWithOTPPostWithHttpInfo( phone: phone, password: password );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'StringBR') as StringBR;
        }
    return Future<StringBR>.value(null);
  }

  /// OTP Login check otp code
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] code:
  ///
  /// * [String] OTP:
  Future<Response> warehouseAuthLoginWithOTPStep2PostWithHttpInfo({ String code, String OTP }) async {
    // Verify required params are set.

    final path = r'/warehouse/auth/LoginWithOTPStep2';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (code != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'Code', code));
    }
    if (OTP != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'OTP', OTP));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// OTP Login check otp code
  ///
  /// Parameters:
  ///
  /// * [String] code:
  ///
  /// * [String] OTP:
  Future<AuthTokenBR> warehouseAuthLoginWithOTPStep2Post({ String code, String OTP }) async {
    final response = await warehouseAuthLoginWithOTPStep2PostWithHttpInfo( code: code, OTP: OTP );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'AuthTokenBR') as AuthTokenBR;
        }
    return Future<AuthTokenBR>.value(null);
  }

  /// Performs an HTTP 'POST /warehouse/auth/Logout' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [String] token:
  Future<Response> warehouseAuthLogoutPostWithHttpInfo({ String token }) async {
    // Verify required params are set.

    final path = r'/warehouse/auth/Logout';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (token != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'token', token));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [String] token:
  Future<BR> warehouseAuthLogoutPost({ String token }) async {
    final response = await warehouseAuthLogoutPostWithHttpInfo( token: token );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BR') as BR;
        }
    return Future<BR>.value(null);
  }
}
