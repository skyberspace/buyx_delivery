//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class WarehouseWarehouseUserApi {
  WarehouseWarehouseUserApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Sets warehouse users Firebase Cloud Messaging Token
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] token:
  Future<Response> warehouseUserSetFirebaseCloudMessagingTokenPostWithHttpInfo({ String token }) async {
    // Verify required params are set.

    final path = r'/warehouseUser/SetFirebaseCloudMessagingToken';

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (token != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'Token', token));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['oauth2'];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Sets warehouse users Firebase Cloud Messaging Token
  ///
  /// Parameters:
  ///
  /// * [String] token:
  Future<BooleanBR> warehouseUserSetFirebaseCloudMessagingTokenPost({ String token }) async {
    final response = await warehouseUserSetFirebaseCloudMessagingTokenPostWithHttpInfo( token: token );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'BooleanBR') as BooleanBR;
        }
    return Future<BooleanBR>.value(null);
  }
}
