//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_warehouse/api.dart';
import 'package:test/test.dart';

// tests for Warehouse
void main() {
  final instance = Warehouse();

  group('test Warehouse', () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // bool isDefault
    test('to test the property `isDefault`', () async {
      // TODO
    });

    // double latitude
    test('to test the property `latitude`', () async {
      // TODO
    });

    // double longitude
    test('to test the property `longitude`', () async {
      // TODO
    });

    // double pricePerKM
    test('to test the property `pricePerKM`', () async {
      // TODO
    });

    // List<ProductWarehouseStock> warehouseStocks (default value: const [])
    test('to test the property `warehouseStocks`', () async {
      // TODO
    });

    // DateTime createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // DateTime updatedAt
    test('to test the property `updatedAt`', () async {
      // TODO
    });

    // bool delete
    test('to test the property `delete`', () async {
      // TODO
    });


  });

}
