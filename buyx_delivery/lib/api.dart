import 'package:buyx_api_warehouse/api.dart';

class Api {
  Api({String baseUrl = "https://buyxapi.skyberspace.com"}) {
    this.client = ApiClient(basePath: baseUrl);
    this.authApi = WarehouseAuthApi(this.client);
    this.orderPreparing = WarehouseOrderPreparingApi(this.client);
    this.warehouseUser = WarehouseWarehouseUserApi(this.client);
  }

  setAsDefaultInstance() {
    Api.instance = this;
  }

  ApiClient client;
  WarehouseAuthApi authApi;
  WarehouseOrderPreparingApi orderPreparing;
  WarehouseWarehouseUserApi warehouseUser;

  static setInstance(Api api) {
    Api.instance = api;
  }

  static Api instance;
}
