import 'package:BuyxCourier/screens/courier/deliveryScreen.dart';
import 'package:BuyxCourier/screens/login/login.dart';
import 'package:BuyxCourier/screens/warehouse/scanBarcode.dart';
import 'package:BuyxCourier/screens/warehouse/wOrderDetails.dart';
import 'package:BuyxCourier/screens/warehouse/warehouseFirst.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:BuyxCourier/config/theme.dart';
import 'package:flutter/services.dart';
import 'api.dart';
import 'models/server/settings.dart';
import 'package:BuyxCourier/models/pref.dart';
import 'package:BuyxCourier/screens/courier/courierMap.dart';
import 'package:BuyxCourier/screens/courier/courierFirst.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

//Account account = Account();
AppThemeData theme = AppThemeData();

var appSettings = AppSettings();
Pref pref = Pref();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
  // background bildirim geldi demek
}

const String delivery_notification_topic = "buyx_delivery_messages";

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'main_notification_channel', // id
  'Buyx Delivert/Packaging Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.max,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> saveTokenToDatabase(String token) async {
  print(token);
  print("token eyy");
  Api.instance.warehouseUser
      .warehouseUserSetFirebaseCloudMessagingTokenPost(token: token);
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  FirebaseMessaging.onMessage.listen((RemoteMessage remoteMessage) {
    flutterLocalNotificationsPlugin.show(
      remoteMessage.notification.hashCode,
      remoteMessage.notification.title,
      remoteMessage.notification.body,
      NotificationDetails(
        iOS: IOSNotificationDetails(),
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channel.description,
          // TODO add a proper drawable resource to android, for now using
          //      one that already exists in example app.
          icon: 'launch_background',
        ),
      ),
    );
  });
  FirebaseMessaging messaging = FirebaseMessaging.instance;
  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );
  //await FirebaseMessaging.instance.subscribeToTopic('buyx_delivery_messages');

  FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    print('Got a message whilst in the foreground!');
    print('Message data: ${message.data}');

    if (message.notification != null) {
      print('Message also contained a notification: ${message.notification}');
    }
  });

  var api = Api();
  api.setAsDefaultInstance();

  runApp(MyApp()
      /*MultiProvider(
      providers: [
        //ChangeNotifierProvider(create: (_) => LoginData()),
      ],
      child: MyApp(),
    ),*/
      );
}

void getFCMToken() async {
  saveTokenToDatabase(await FirebaseMessaging.instance.getToken());
  FirebaseMessaging.instance.onTokenRefresh.listen(saveTokenToDatabase);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      title: 'Buyx Courier',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: "/login", //'/splash'
      routes: {
        //'/splash': (BuildContext context) => SplashScreen(),
        '/login': (BuildContext context) => LoginScreen(),
        "/warehouseFirst": (BuildContext context) => WarehouseFirst(),
        "/deliveryScreen": (BuildContext context) => DeliveryScreen(),
        "/courierMap": (BuildContext context) => CourierMap(),
        '/courierFirst': (BuildContext context) => CourierFirst(),
        '/scanBarcode': (BuildContext context) => ScanBarcode(),
        "/detay": (BuildContext context) => WOrderDetails(),
        //'/main': (BuildContext context) => MainScreen(),
      },
    );
  }
}

dprint(String str) {
  if (!kReleaseMode) print(str);
}
