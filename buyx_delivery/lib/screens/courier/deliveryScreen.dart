import 'package:BuyxCourier/screens/courier/courierMap.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:background_location/background_location.dart';
import 'package:buyx_api_warehouse/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:BuyxCourier/ui/widgets/ICard22.dart';
import 'package:BuyxCourier/ui/widgets/ibackbutton.dart';
import 'package:BuyxCourier/main.dart';
import 'package:BuyxCourier/ui/widgets/ICard23.dart';
import 'package:BuyxCourier/ui/widgets/ICard24.dart';
import 'package:BuyxCourier/ui/widgets/ibutton3.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

import '../../api.dart';
import 'courierFirst.dart';

// ignore: must_be_immutable
class DeliveryScreen extends StatefulWidget {
  DeliveryScreen({this.orderId});
  int orderId;
  /*final Function(String) callback;
  DeliveryScreen({Key key, this.callback}) : super(key: key);*/

  @override
  _DeliveryScreenState createState() => _DeliveryScreenState();
}

class _DeliveryScreenState extends State<DeliveryScreen> {
  ///////////////////////////////////////////////////////////////////////////////
  double windowWidth = 0.0;
  double windowHeight = 0.0;

  List<OrderLine> orderContents;

  Order deliveryOrderSubjects;
  bool ready = false;
  bool acceptButtonAlive = true;

  void acceptButtonAliveCheck() {
    if (deliveryOrderSubjects.deliveryAgentId != null)
      acceptButtonAlive = false;
  }

  void load() async {
    try {
      await getOrderContents();
      acceptButtonAliveCheck();
      setState(() {
        ready = true;
      });
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  //var deliveryOrderSubjectsDetails;
  getOrderContents(/*String token, success, error*/) async {
    try {
      var orders = await Api.instance.orderPreparing
          .orderPreparingGetOrderFromIdPost(
              orderIdRequest: OrderIdRequest(orderId: widget.orderId));
      orderContents = orders.data.lines;
      deliveryOrderSubjects = orders.data;
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  @override
  void initState() {
    super.initState();
    load();
  }

  //

  @override
  Widget build(BuildContext context) {
    windowWidth = MediaQuery.of(context).size.width;
    windowHeight = MediaQuery.of(context).size.height;
    if (!ready)
      return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CupertinoActivityIndicator(
                  animating: true,
                ),
                Text("Loading"),
              ],
            )
          ],
        ),
      );
    else if (ready)
      return WillPopScope(
        onWillPop: () async {
          Navigator.pop(context, "");
          return false;
        },
        child: Scaffold(
          body: Stack(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(
                    left: 10,
                    right: 10,
                    top: MediaQuery.of(context).padding.top + 50),
                child: _body(),
              ),
              Container(
                margin: EdgeInsets.only(left: 5, top: 10),
                alignment: Alignment.topLeft,
                child: IBackButton(
                    onBackClick: () {
                      Navigator.pop(context);
                    },
                    color: theme.colorPrimary,
                    iconColor: Colors.white),
              ),
            ],
          ),
        ),
      );
  }

  _body() {
    return ListView(
      children: _body2(),
    );
  }

  _body2() {
    List<Widget> list = [];

    //for (var deliveryOrderSubjects in orders) {
    if (true /*account.currentOrder == deliveryOrderSubjects.id*/) {
      /* var distance =
          "${(deliveryOrderSubjects.distance / 1000).toStringAsFixed(3)} km";*/ // km or miles
      /*if (appSettings.distanceUnit == "mi")
        distance =
            "${(deliveryOrderSubjects[0].distance / 1609).toStringAsFixed(3)} ${appSettings.distanceUnit}";*/ // km or miles
      list.add(ICard22(
        color: theme.colorBackgroundDialog,
        colorRoute: theme.colorPrimary,
        id: deliveryOrderSubjects.id,
        text: "Order Id : ${deliveryOrderSubjects.id}", // Order ID122
        textStyle: theme.text18boldPrimary,
        text2:
            "Date: ${DateFormat("dd-MM-yyyy kk:mm").format(deliveryOrderSubjects.deliveredAt.toLocal())}", // Date: 2020-07-08 12:35
        text2Style: theme.text14,
        text3: "",
        text3Style: theme.text18bold,
        text4: /*deliveryOrderSubjects.method*/ "", // cache on delivery
        text4Style: theme.text14,
        text5: "Customer   and", // Distance
        text5Style: theme.text16,
        text6: "Pick Up Adresses",
        text6Style: theme.text18boldPrimary,
        text7: deliveryOrderSubjects.orderAddress.addressLine1 != null
            ? deliveryOrderSubjects.orderAddress.addressLine1
            : "",
        text7Style: theme.text14,
        text8: deliveryOrderSubjects.pickupAddress.addressLine1 != null
            ? deliveryOrderSubjects.pickupAddress.addressLine1
            : "",
        text8Style: theme.text14,
        //button1Enable: (status == 0 || status == 1),
        button1Enable: acceptButtonAlive,
        button2Enable: true,
        button1Text: "On Map", // On Map
        button1Style: TextStyle(color: Colors.black),
        button2Text:
            "Accept", //(status == 0) ? strings.get(48) : strings.get(51),
        // Accept or Complete
        button2Style: theme.text14boldWhite,
        callbackButton1: _onAcceptClick,
        callbackButton2: _onMap,
      ));

      list.add(
        ICard23(
          text: "Customer Name:", // Customer name
          textStyle: theme.text14,
          text2: "Customer Phone:", // "Customer phone",
          text2Style: theme.text14,
          text3: deliveryOrderSubjects.user.name +
              deliveryOrderSubjects.user.surname,
          text3Style: theme.text16bold,
          text4: deliveryOrderSubjects.user.phone,
          text4Style: theme.text16bold,
        ),
      );

      list.add(
        SizedBox(
          height: 10,
        ),
      );

      list.add(
        Container(
          alignment: Alignment.center,
          child: IButton3(
              color: theme.colorPrimary,
              text: "Call Customer", // "Call to Customer",
              textStyle: theme.text14boldWhite,
              pressButton: _onCallToCustomer),
        ),
      );

      list.add(
        SizedBox(
          height: 10,
        ),
      );

      var deliveryOrderSubjectsDetails = List<ICard24Data>();
      for (var _details in orderContents) {
        try {
          if (orderContents.length != 0)
            deliveryOrderSubjectsDetails.add(ICard24Data(
                /*deliveryOrderSubjects.currency*/ "£",
                _details.product.images.length > 0
                    ? Api.instance.client.basePath +
                        "/cdn/image/150x150" +
                        _details.product.images.first.path
                    : Api.instance.client.basePath +
                        "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                "${_details.productNameCache} X ${_details.quantity}",
                /*_details.count*/ 1,
                _details.price.toString()));
        } catch (e) {
          snackBar("Unknown Problem. Please try again");
        }
        /*else if (_details.extrasCount != 0)
          deliveryOrderSubjectsDetails.add(ICard24Data(
              deliveryOrderSubjects.currency,
              "$serverImages${_details.image}",
              _details.extras,
              _details.extrasCount,
              _details.extrasPrice));*/
      }

      /*var textTotal = (appSettings.rightSymbol == "false")
          ? "${deliveryOrderSubjects.currency}${deliveryOrderSubjects.total.toStringAsFixed(appSettings.symbolDigits)}"
          : "${deliveryOrderSubjects.total.toStringAsFixed(appSettings.symbolDigits)}${deliveryOrderSubjects.currency}";*/

      list.add(
        ICard24(
          color: theme.colorBackgroundDialog,
          text: "Order Details:", // Order Details
          textStyle: theme.text18boldPrimary,
          text2Style: theme.text16,
          colorProgressBar: theme.colorPrimary,
          data: deliveryOrderSubjectsDetails,
          text3Style: theme.text16,
          text3: "${deliveryOrderSubjects.subTotal}", // Subtotal
          text4: "Delivery Fee:", // Shopping costs
          fee: "${deliveryOrderSubjects.deliveryFee}",
          percent: "deliveryOrderSubjects.percent",
          text5: "Taxes:", // Taxes
          tax: "Tax: ${deliveryOrderSubjects.tax}",
          text6: "Total: ${deliveryOrderSubjects.total}", // Total
          text6Style: theme.text16bold,
        ),
      );
      print(
          "tax :  ${deliveryOrderSubjects.tax} , total : ${deliveryOrderSubjects.total} , deliveryfee : ${deliveryOrderSubjects.deliveryFee}, subtotal : ${deliveryOrderSubjects.subTotal} ");

      list.add(
        SizedBox(
          height: 10,
        ),
      );

      list.add(
        Container(
          child: Column(
            children: [
              Text(
                "Notes",
                style: TextStyle(color: Colors.red),
              ),
              if (deliveryOrderSubjects.note != null) ...{
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("' ${deliveryOrderSubjects.note} '"),
                ),
              } else ...{
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Text("No Note"),
                ),
              }
            ],
          ),
        ),
      );

      list.add(
        SizedBox(
          height: 10,
        ),
      );

      list.add(
        Container(
          alignment: Alignment.center,
          child: IButton3(
              color: Colors.redAccent,
              text: "Order is Delivered",
              textStyle: theme.text14boldWhite,
              pressButton: () async {
                try {
                  var deliverResult = await Api.instance.orderPreparing
                      .orderPreparingOrderDeliveredPost(
                          assignRequest:
                              AssignRequest(orderId: this.widget.orderId));
                  if (deliverResult.success) {
                    await BackgroundLocation.stopLocationService();
                    //
                    return showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(
                            "Order Delivery Status!",
                            style: TextStyle(color: Colors.redAccent),
                          ),
                          content: Text(
                            "Success! The order ${widget.orderId} has been successfully delivered.",
                          ),
                          actions: <Widget>[
                            new TextButton(
                              child: new Text(
                                "OK",
                              ),
                              onPressed: () {
                                Navigator.pushAndRemoveUntil(context,
                                    MaterialPageRoute(builder: (context) {
                                  return CourierFirst();
                                }), (route) => false);
                              },
                            ),
                          ],
                        );
                      },
                    );
                    //
                  } else if (!deliverResult.success)
                    return showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: Text(
                            "Wrong Action!",
                            style: TextStyle(color: Colors.redAccent),
                          ),
                          content: Text(
                            /*"Warning! You have not accepted the order ${widget.orderId} yet!"*/ deliverResult
                                .errors[0],
                          ),
                          actions: <Widget>[
                            new TextButton(
                              child: new Text(
                                "OK",
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  // buton içindeki to do burası.
                } catch (e) {
                  return showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text(
                          "Connection Problem!",
                          style: TextStyle(color: Colors.redAccent),
                        ),
                        content: Text(
                          "Warning! There is a connection problem. Please try again.",
                        ),
                        actions: <Widget>[
                          new TextButton(
                            child: new Text(
                              "OK",
                            ),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                }
              }),
        ),
      );

      list.add(
        SizedBox(
          height: 100,
        ),
      );
    }
    //}
    return list;
  }

  _onCallToCustomer() async {
    try {
      var uri = 'tel:${deliveryOrderSubjects.user.phone}';
      if (await canLaunch(uri)) await launch(uri);
    } catch (e) {
      return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Connection Problem!",
              style: TextStyle(color: Colors.redAccent),
            ),
            content: Text(
              "Warning! There is a connection problem. Please try again.",
            ),
            actions: <Widget>[
              new TextButton(
                child: new Text(
                  "OK",
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  _onMap(int orderId) {
    print("asfdgfdgjhfgdsggfjh");
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CourierMap(
          orderId: orderId,
        ),
      ),
    );
  }

  _onAcceptClick(int orderId) async {
    try {
      var acceptResult = await Api.instance.orderPreparing
          .orderPreparingAssignDeliveryToMePost(
              assignRequest: AssignRequest(orderId: orderId));
      if (acceptResult.success) {
        await BackgroundLocation.startLocationService(distanceFilter: 20);
        BackgroundLocation.getLocationUpdates((location) async {
          String latitude = location.latitude.toString();
          String longitude = location.longitude.toString();
          String accuracy = location.accuracy.toString();
          String altitude = location.altitude.toString();
          String bearing = location.bearing.toString();
          await Api.instance.orderPreparing
              .orderPreparingSetOrderDeliveryLastLocationPost(
                  orderDeliveryLocationRequest: OrderDeliveryLocationRequest(
                      orderId: widget.orderId,
                      latitude: location.latitude,
                      longutide: location.longitude));
        });
        setState(() {
          acceptButtonAlive = false;
        });
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Order Assign Status!",
                style: TextStyle(color: Colors.redAccent),
              ),
              content: Text(
                "The order $orderId is assigned to you!",
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "OK",
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      } else if (!acceptResult.success)
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Order Assign Status!",
                style: TextStyle(color: Colors.redAccent),
              ),
              content: Text(
                /*"Warning! This order is either assigned or cancelled. Please turn back to new deliveries page."*/
                acceptResult.errors[0],
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "OK",
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }
}
