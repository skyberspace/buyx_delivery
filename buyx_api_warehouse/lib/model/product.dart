//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Product {
  /// Returns a new [Product] instance.
  Product({
    this.id,
    this.name,
    this.description,
    this.price,
    this.shortDescription,
    this.minCartCount,
    this.active,
    this.barcode,
    this.metaTags,
    this.metaDescription,
    this.images,
    this.stocks,
    this.categories,
    this.deletedAt,
    this.isDeleted = false,
    this.createdAt,
    this.updatedAt,
    this.delete,
  });

  int id;

  String name;

  String description;

  double price;

  String shortDescription;

  double minCartCount;

  bool active;

  String barcode;

  String metaTags;

  String metaDescription;

  List<ProductImage> images;

  List<ProductWarehouseStock> stocks;

  List<ProductCategory> categories;

  DateTime deletedAt;

  bool isDeleted;

  DateTime createdAt;

  DateTime updatedAt;

  bool delete;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Product &&
     other.id == id &&
     other.name == name &&
     other.description == description &&
     other.price == price &&
     other.shortDescription == shortDescription &&
     other.minCartCount == minCartCount &&
     other.active == active &&
     other.barcode == barcode &&
     other.metaTags == metaTags &&
     other.metaDescription == metaDescription &&
     other.images == images &&
     other.stocks == stocks &&
     other.categories == categories &&
     other.deletedAt == deletedAt &&
     other.isDeleted == isDeleted &&
     other.createdAt == createdAt &&
     other.updatedAt == updatedAt &&
     other.delete == delete;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (price == null ? 0 : price.hashCode) +
    (shortDescription == null ? 0 : shortDescription.hashCode) +
    (minCartCount == null ? 0 : minCartCount.hashCode) +
    (active == null ? 0 : active.hashCode) +
    (barcode == null ? 0 : barcode.hashCode) +
    (metaTags == null ? 0 : metaTags.hashCode) +
    (metaDescription == null ? 0 : metaDescription.hashCode) +
    (images == null ? 0 : images.hashCode) +
    (stocks == null ? 0 : stocks.hashCode) +
    (categories == null ? 0 : categories.hashCode) +
    (deletedAt == null ? 0 : deletedAt.hashCode) +
    (isDeleted == null ? 0 : isDeleted.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (updatedAt == null ? 0 : updatedAt.hashCode) +
    (delete == null ? 0 : delete.hashCode);

  @override
  String toString() => 'Product[id=$id, name=$name, description=$description, price=$price, shortDescription=$shortDescription, minCartCount=$minCartCount, active=$active, barcode=$barcode, metaTags=$metaTags, metaDescription=$metaDescription, images=$images, stocks=$stocks, categories=$categories, deletedAt=$deletedAt, isDeleted=$isDeleted, createdAt=$createdAt, updatedAt=$updatedAt, delete=$delete]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (description != null) {
      json[r'description'] = description;
    }
    if (price != null) {
      json[r'price'] = price;
    }
    if (shortDescription != null) {
      json[r'shortDescription'] = shortDescription;
    }
    if (minCartCount != null) {
      json[r'minCartCount'] = minCartCount;
    }
    if (active != null) {
      json[r'active'] = active;
    }
    if (barcode != null) {
      json[r'barcode'] = barcode;
    }
    if (metaTags != null) {
      json[r'metaTags'] = metaTags;
    }
    if (metaDescription != null) {
      json[r'metaDescription'] = metaDescription;
    }
    if (images != null) {
      json[r'images'] = images;
    }
    if (stocks != null) {
      json[r'stocks'] = stocks;
    }
    if (categories != null) {
      json[r'categories'] = categories;
    }
    if (deletedAt != null) {
      json[r'deletedAt'] = deletedAt.toUtc().toIso8601String();
    }
    if (isDeleted != null) {
      json[r'isDeleted'] = isDeleted;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt.toUtc().toIso8601String();
    }
    if (updatedAt != null) {
      json[r'updatedAt'] = updatedAt.toUtc().toIso8601String();
    }
    if (delete != null) {
      json[r'_delete'] = delete;
    }
    return json;
  }

  /// Returns a new [Product] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Product fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Product(
        id: json[r'id'],
        name: json[r'name'],
        description: json[r'description'],
        price: json[r'price'],
        shortDescription: json[r'shortDescription'],
        minCartCount: json[r'minCartCount'],
        active: json[r'active'],
        barcode: json[r'barcode'],
        metaTags: json[r'metaTags'],
        metaDescription: json[r'metaDescription'],
        images: ProductImage.listFromJson(json[r'images']),
        stocks: ProductWarehouseStock.listFromJson(json[r'stocks']),
        categories: ProductCategory.listFromJson(json[r'categories']),
        deletedAt: json[r'deletedAt'] == null
          ? null
          : DateTime.parse(json[r'deletedAt']),
        isDeleted: json[r'isDeleted'],
        createdAt: json[r'createdAt'] == null
          ? null
          : DateTime.parse(json[r'createdAt']),
        updatedAt: json[r'updatedAt'] == null
          ? null
          : DateTime.parse(json[r'updatedAt']),
        delete: json[r'_delete'],
    );

  static List<Product> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Product>[]
      : json.map((v) => Product.fromJson(v)).toList(growable: true == growable);

  static Map<String, Product> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Product>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Product.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Product-objects as value to a dart map
  static Map<String, List<Product>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Product>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Product.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

