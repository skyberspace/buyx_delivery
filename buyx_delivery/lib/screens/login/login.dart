import 'package:BuyxCourier/api.dart';
import 'package:BuyxCourier/screens/courier/deliveryScreen.dart';
import 'package:BuyxCourier/screens/warehouse/warehouseFirst.dart';
import 'package:BuyxCourier/screens/courier/courierFirst.dart';
import 'package:BuyxCourier/services/globals.dart';
import 'package:BuyxCourier/ui/widgets/colorloader2.dart';
import 'package:BuyxCourier/ui/widgets/easyDialog2.dart';
import 'package:BuyxCourier/ui/widgets/ibutton2.dart';
import 'package:BuyxCourier/ui/widgets/ibutton3.dart';
import 'package:BuyxCourier/utilities/constants.dart';
import 'package:buyx_api_warehouse/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:BuyxCourier/main.dart';
import 'package:BuyxCourier/ui/widgets/ibackground4.dart';
import 'package:BuyxCourier/ui/widgets/iinputField2Password.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:BuyxCourier/screens/login/pinCodeVerify.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  String phone;
  bool wait = true;

  _pressLoginButton() async {
    try {
      _waits(true);
      StringBR result = await Api.instance.authApi
          .warehouseAuthLoginWithOTPPost(
              password: editControllerPassword.text, phone: this.phone);
      if (result.success == true) {
        await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) {
            return PinCodeVerificationScreen(this.phone, result);
          }),
        );
      } else {
        _waits(false);
        snackBar(result.errors[0]);
      }
    } catch (e) {
      _waits(false);
      snackBar("Unknown Problem. Please try again");
    }
  }

  var windowWidth;
  var windowHeight;
  final editControllerName = TextEditingController();
  final editControllerPassword = TextEditingController();
  bool _wait = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    editControllerName.dispose();
    editControllerPassword.dispose();
    super.dispose();
  }

  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }

  _waits(bool value) {
    if (mounted)
      setState(() {
        _wait = value;
      });
    _wait = value;
  }

  @override
  Widget build(BuildContext context) {
    windowWidth = MediaQuery.of(context).size.width;
    windowHeight = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Colors.white12, //theme.colorBackground,
        body: Directionality(
          textDirection: TextDirection.ltr,
          child: Stack(
            children: <Widget>[
              IBackground4(
                width: windowWidth,
                colorsGradient: [
                  Colors.white10,
                  Colors.white10
                ], /*theme.colorsGradient*/
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                width: windowWidth,
                alignment: Alignment.bottomCenter,
                child: _body(),
              ),
              if (_wait)
                (Container(
                  color: Color(0x80000000),
                  width: windowWidth,
                  height: windowHeight,
                  child: Center(
                    child: ColorLoader2(
                      color1: theme.colorPrimary,
                      color2: theme.colorCompanion,
                      color3: theme.colorPrimary,
                    ),
                  ),
                )),
            ],
          ),
        ));
  }

  _body() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          width: windowWidth * 0.4,
          height: windowWidth * 0.4,
          child: Image.asset("assets/logo.png", fit: BoxFit.contain),
        ),
        SizedBox(
          height: windowHeight * 0.1,
        ),
        Container(
          margin: EdgeInsets.only(left: 15, right: 15),
          child: Text("Welcome Buyx Warehouse", // "Let's start with LogIn!"
              style: theme.text20boldWhite),
        ),
        SizedBox(
          height: 15,
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          height: 0.5,
          color: Colors.white.withAlpha(200), // line
        ),
        SizedBox(
          height: 15,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: IntlPhoneField(
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white54),
            decoration: InputDecoration(
              labelStyle: TextStyle(color: Colors.white54),
              counterStyle: TextStyle(color: Colors.white54),
              prefixStyle: TextStyle(color: Colors.white54),
              labelText: 'Phone Number',
              border: OutlineInputBorder(
                borderSide: BorderSide(),
              ),
            ),
            initialCountryCode: 'GB',
            onChanged: (phone) {
              this.phone = phone.completeNumber;
              print(phone
                  .completeNumber); // When sending message from otp, use the phone.completeNumber
            },
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          height: 0.5,
          color: Colors.white.withAlpha(200), // line
        ),
        SizedBox(
          height: 5,
        ),
        Container(
            margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: IInputField2Password(
              hint: "Password", // "Password"
              icon: Icons.vpn_key,
              colorDefaultText: Colors.white54,
              controller: editControllerPassword,
            )),
        Container(
          margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
          height: 0.5,
          color: Colors.white.withAlpha(200), // line
        ),
        SizedBox(
          height: 30,
        ),
        Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: IButton3(
            pressButton: _pressLoginButton, text: "LOGIN", // LOGIN
            color: theme.colorCompanion,
            textStyle: theme.text16boldWhite,
          ),
        ),
        SizedBox(
          height: 15,
        ),
      ],
    );
  }
}
