import 'package:flutter/material.dart';

const kTempTextStyle = TextStyle(
  fontFamily: 'Comfortaa-Regular',
  fontSize: 100.0,
);

const kZodiacNamesTextStyle = TextStyle(
    fontFamily: 'Comfortaa-Regular', fontSize: 15.0, color: Color(0xffD0CCCC));

const kButtonTextStyle = TextStyle(
  fontSize: 30.0,
  fontFamily: 'Comfortaa-Regular',
);

const kChooseWriting = TextStyle(
    fontFamily: 'Comfortaa-Regular', color: Color(0xffD0CCCC), fontSize: 40.0);

const kConditionTextStyle = TextStyle(
  fontSize: 100.0,
);

const kNormalSizeText =
    TextStyle(fontSize: 15.0, fontFamily: 'Comfortaa-Regular');
