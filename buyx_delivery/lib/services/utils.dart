double toDouble(String str) {
  double ret = 0;
  try {
    ret = double.parse(str);
  } catch (_) {}
  return ret;
}

int toInt(String str) {
  int ret = 0;
  try {
    ret = int.parse(str);
  } catch (_) {}
  return ret;
}

bool toBool(String str) {
  return (str == "true") ? true : false;
}
