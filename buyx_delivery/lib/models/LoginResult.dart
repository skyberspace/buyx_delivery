class LoginResult {
  bool success;
  List<String> errors;
  List<String> infos;
  AuthToken data;

  LoginResult({this.success, this.errors, this.infos, this.data});

  static LoginResult fromJson(Map<String, dynamic> json) {
    return LoginResult.fromJson2(json);
  }

  LoginResult.fromJson2(Map<String, dynamic> json) {
    success = json['success'];
    errors = json['errors'].cast<String>();
    infos = json['infos'].cast<String>();
    data = json['data'] != null ? new AuthToken.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['errors'] = this.errors;
    data['infos'] = this.infos;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class AuthToken {
  int id;
  String token;
  String expires;
  int userId;
  AuthTokenUser user;
  String createdAt;
  String updatedAt;
  bool bDelete;

  AuthToken(
      {this.id,
      this.token,
      this.expires,
      this.userId,
      this.user,
      this.createdAt,
      this.updatedAt,
      this.bDelete});

  AuthToken.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    token = json['token'];
    expires = json['expires'];
    userId = json['userId'];
    user =
        json['user'] != null ? new AuthTokenUser.fromJson(json['user']) : null;
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    bDelete = json['_delete'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['token'] = this.token;
    data['expires'] = this.expires;
    data['userId'] = this.userId;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['_delete'] = this.bDelete;
    return data;
  }
}

class AuthTokenUser {
  int id;
  String email;
  String name;
  String surname;
  String phone;
  bool marketingEmails;
  bool marketingNotifications;
  bool marketingSMS;
  bool marketingCalls;
  bool isGuest;
  String createdAt;
  String updatedAt;
  bool bDelete;

  AuthTokenUser(
      {this.id,
      this.email,
      this.name,
      this.surname,
      this.phone,
      this.marketingEmails,
      this.marketingNotifications,
      this.marketingSMS,
      this.marketingCalls,
      this.isGuest,
      this.createdAt,
      this.updatedAt,
      this.bDelete});

  AuthTokenUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    email = json['email'];
    name = json['name'];
    surname = json['surname'];
    phone = json['phone'];
    marketingEmails = json['marketingEmails'];
    marketingNotifications = json['marketingNotifications'];
    marketingSMS = json['marketingSMS'];
    marketingCalls = json['marketingCalls'];
    isGuest = json['isGuest'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    bDelete = json['_delete'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['email'] = this.email;
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['phone'] = this.phone;
    data['marketingEmails'] = this.marketingEmails;
    data['marketingNotifications'] = this.marketingNotifications;
    data['marketingSMS'] = this.marketingSMS;
    data['marketingCalls'] = this.marketingCalls;
    data['isGuest'] = this.isGuest;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['_delete'] = this.bDelete;
    return data;
  }
}
