//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Status {
  /// Returns a new [Status] instance.
  Status({
    this.id,
    this.type,
    this.subtype,
  });

  String id;

  String type;

  String subtype;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Status &&
     other.id == id &&
     other.type == type &&
     other.subtype == subtype;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (type == null ? 0 : type.hashCode) +
    (subtype == null ? 0 : subtype.hashCode);

  @override
  String toString() => 'Status[id=$id, type=$type, subtype=$subtype]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (type != null) {
      json[r'type'] = type;
    }
    if (subtype != null) {
      json[r'subtype'] = subtype;
    }
    return json;
  }

  /// Returns a new [Status] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Status fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Status(
        id: json[r'id'],
        type: json[r'type'],
        subtype: json[r'subtype'],
    );

  static List<Status> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Status>[]
      : json.map((v) => Status.fromJson(v)).toList(growable: true == growable);

  static Map<String, Status> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Status>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Status.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Status-objects as value to a dart map
  static Map<String, List<Status>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Status>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Status.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

