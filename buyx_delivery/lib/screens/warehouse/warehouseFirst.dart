import 'package:BuyxCourier/screens/warehouse/wOrderDetails.dart';
import 'package:buyx_api_warehouse/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:BuyxCourier/api.dart';
import 'package:intl/intl.dart';

class WarehouseFirst extends StatefulWidget {
  @override
  _WarehouseFirstState createState() => _WarehouseFirstState();
}

class _WarehouseFirstState extends State<WarehouseFirst> {
  bool ready = false;

  List<Order> historyList;

  List<Order> showList;
  //int _currentTabIndex = 0;
  //TabController _tabController;
  Future<void> getOrders(/*String token, success, error*/) async {
    try {
      var orders = await Api.instance.orderPreparing
          .orderPreparingGetOrdersForPreparingPost();
      return {
        showList = orders.data,
      };
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  Future<void> getHistoryOrders(/*String token, success, error*/) async {
    try {
      var orders = await Api.instance.orderPreparing
          .orderPreparingGetPackagingHistoryPost();
      return {
        historyList = orders.data,
      };
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  void _activeOrderCheck() async {
    try {
      OrderBR activeOrderCheck =
          await Api.instance.orderPreparing.orderPreparingGetCurrentOrderPost();
      if (activeOrderCheck.success) {
        if (activeOrderCheck.data == null) return null;
        if (activeOrderCheck.data != null) {
          await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return WOrderDetails(
                orderId: activeOrderCheck.data.id,
              );
            }),
          );
          load();
          return null;
        }
      }
      /*else if (!activeOrderCheck.success) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(
                "Status!",
                style: TextStyle(color: Colors.redAccent),
              ),
              content: Text(
                activeOrderCheck.errors[0],
              ),
              actions: <Widget>[
                new TextButton(
                  child: new Text(
                    "Ok",
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                    load();
                  },
                ),
              ],
            );
          },
        );
      }*/
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  @override
  void initState() {
    super.initState();
    load();
  }

  Future<void> load() async {
    _activeOrderCheck();
    await getOrders();
    await getHistoryOrders();
    setState(() {
      ready = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black54,
          bottom: TabBar(
            tabs: [
              Tab(
                text: "New",
              ),
              Tab(
                text: "History",
              ),
            ],
          ),
          title: Text('Warehouse Orders'),
        ),
        body: TabBarView(
          children: [
            if (!ready) ...{
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CupertinoActivityIndicator(
                        animating: true,
                      ),
                      Text("Loading"),
                    ],
                  )
                ],
              ),
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CupertinoActivityIndicator(
                        animating: true,
                      ),
                      Text("Loading"),
                    ],
                  )
                ],
              )
            } else ...{
              RefreshIndicator(
                onRefresh: load,
                child: ListView.builder(
                    itemCount: showList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new Card(
                        child: new Container(
                          padding: new EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              new ListTile(
                                //activeColor: Colors.pink[300],
                                dense: true,
                                subtitle: Text(showList[index].user.name +
                                    " " +
                                    showList[index].user.surname),
                                //font change
                                title: new Text(
                                  "${showList[index].id}",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5),
                                ),
                                leading: Text("${showList[index].total}" + "£"),
                                onTap: () async {
                                  /*var assignResult = await Api
                                      .instance.orderPreparing
                                      .orderPreparingAssignPackagingToMePost(
                                          assignRequest: AssignRequest(
                                              orderId: showList[index].id));*/
                                  if (/*assignResult.success*/ true) {
                                    await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => WOrderDetails(
                                          orderId: showList[index].id,
                                        ),
                                      ),
                                    );
                                    load();
                                  }
                                  /*else {
                                    return showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text(
                                            "Status!",
                                            style: TextStyle(
                                                color: Colors.redAccent),
                                          ),
                                          content: Text(
                                            "The order ${showList[index].id} is busy. Please refresh the page!",
                                          ),
                                          actions: <Widget>[
                                            new TextButton(
                                              child: new Text(
                                                "OK",
                                              ),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  }*/
                                },
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              ),
              RefreshIndicator(
                onRefresh: getHistoryOrders,
                child: ListView.builder(
                    itemCount: historyList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new Card(
                        child: new Container(
                          padding: new EdgeInsets.all(10.0),
                          child: Column(
                            children: <Widget>[
                              new ListTile(
                                //activeColor: Colors.pink[300],
                                dense: true,
                                subtitle: Text(historyList[index].user.name +
                                    historyList[index].user.surname),
                                //font change
                                title: new Text(
                                  "${historyList[index].id}",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      letterSpacing: 0.5),
                                ),
                                leading: Text("${historyList[index].total}£"),
                                onTap: () {
                                  return showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: Text(
                                          "History Order Detail!",
                                          style: TextStyle(
                                              color: Colors.redAccent),
                                        ),
                                        content: Text(
                                          "The  order ${historyList[index].id} is packaged at ${DateFormat("dd-MM-yyyy kk:mm").format(historyList[index].packagedAt.toLocal())}!",
                                        ),
                                        actions: <Widget>[
                                          new TextButton(
                                            child: new Text(
                                              "OK",
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                      );
                    }),
              ),
            }
          ],
        ),
      ),
    );
  }

  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }
}
