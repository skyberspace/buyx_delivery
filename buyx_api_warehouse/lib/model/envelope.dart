//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Envelope {
  /// Returns a new [Envelope] instance.
  Envelope({
    this.isNull,
    this.width,
    this.height,
    this.minX,
    this.maxX,
    this.minY,
    this.maxY,
    this.area,
    this.minExtent,
    this.maxExtent,
    this.centre,
  });

  bool isNull;

  double width;

  double height;

  double minX;

  double maxX;

  double minY;

  double maxY;

  double area;

  double minExtent;

  double maxExtent;

  Coordinate centre;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Envelope &&
     other.isNull == isNull &&
     other.width == width &&
     other.height == height &&
     other.minX == minX &&
     other.maxX == maxX &&
     other.minY == minY &&
     other.maxY == maxY &&
     other.area == area &&
     other.minExtent == minExtent &&
     other.maxExtent == maxExtent &&
     other.centre == centre;

  @override
  int get hashCode =>
    (isNull == null ? 0 : isNull.hashCode) +
    (width == null ? 0 : width.hashCode) +
    (height == null ? 0 : height.hashCode) +
    (minX == null ? 0 : minX.hashCode) +
    (maxX == null ? 0 : maxX.hashCode) +
    (minY == null ? 0 : minY.hashCode) +
    (maxY == null ? 0 : maxY.hashCode) +
    (area == null ? 0 : area.hashCode) +
    (minExtent == null ? 0 : minExtent.hashCode) +
    (maxExtent == null ? 0 : maxExtent.hashCode) +
    (centre == null ? 0 : centre.hashCode);

  @override
  String toString() => 'Envelope[isNull=$isNull, width=$width, height=$height, minX=$minX, maxX=$maxX, minY=$minY, maxY=$maxY, area=$area, minExtent=$minExtent, maxExtent=$maxExtent, centre=$centre]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (isNull != null) {
      json[r'isNull'] = isNull;
    }
    if (width != null) {
      json[r'width'] = width;
    }
    if (height != null) {
      json[r'height'] = height;
    }
    if (minX != null) {
      json[r'minX'] = minX;
    }
    if (maxX != null) {
      json[r'maxX'] = maxX;
    }
    if (minY != null) {
      json[r'minY'] = minY;
    }
    if (maxY != null) {
      json[r'maxY'] = maxY;
    }
    if (area != null) {
      json[r'area'] = area;
    }
    if (minExtent != null) {
      json[r'minExtent'] = minExtent;
    }
    if (maxExtent != null) {
      json[r'maxExtent'] = maxExtent;
    }
    if (centre != null) {
      json[r'centre'] = centre;
    }
    return json;
  }

  /// Returns a new [Envelope] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Envelope fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Envelope(
        isNull: json[r'isNull'],
        width: json[r'width'],
        height: json[r'height'],
        minX: json[r'minX'],
        maxX: json[r'maxX'],
        minY: json[r'minY'],
        maxY: json[r'maxY'],
        area: json[r'area'],
        minExtent: json[r'minExtent'],
        maxExtent: json[r'maxExtent'],
        centre: Coordinate.fromJson(json[r'centre']),
    );

  static List<Envelope> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Envelope>[]
      : json.map((v) => Envelope.fromJson(v)).toList(growable: true == growable);

  static Map<String, Envelope> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Envelope>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Envelope.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Envelope-objects as value to a dart map
  static Map<String, List<Envelope>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Envelope>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Envelope.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

