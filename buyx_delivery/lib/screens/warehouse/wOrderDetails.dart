import 'package:BuyxCourier/screens/warehouse/scanBarcode.dart';
import 'package:BuyxCourier/screens/warehouse/warehouseFirst.dart';
import 'package:BuyxCourier/services/globals.dart';
import 'package:buyx_api_warehouse/api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../api.dart';
import 'package:flutter_html/flutter_html.dart';

// ignore: must_be_immutable
class WOrderDetails extends StatefulWidget {
  WOrderDetails({this.orderId});
  final int orderId;
  @override
  _WOrderDetailsState createState() => _WOrderDetailsState();
}

class _WOrderDetailsState extends State<WOrderDetails> {
  List<OrderLine> orderContentsList;

  //var orderContentsList;
  bool ready = false;

  Order order;

  getOrderContents(/*String token, success, error*/) async {
    try {
      var orders = await Api.instance.orderPreparing
          .orderPreparingGetOrderFromIdPost(
              orderIdRequest: OrderIdRequest(orderId: widget.orderId));

      this.order = orders.data;
      orderContentsList = orders.data.lines;
      setState(() {
        this.ready = true;
      });
    } catch (e) {
      // TODO
    }
  }

  @override
  void initState() {
    super.initState();

    getOrderContents();
  }

  @override
  Widget build(BuildContext context) {
    if (!ready) {
      return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CupertinoActivityIndicator(
                  animating: true,
                ),
                Text("Loading"),
              ],
            )
          ],
        ),
      );
    } else if (ready) {
      return WillPopScope(
        onWillPop: () async {
          Navigator.pop(context, "");
          return false;
        },
        child: Scaffold(
          appBar: AppBar(
            actions: [
              if (order.packagingAgentId == null)
                TextButton(
                  child: Text("Accept"),
                  onPressed: () async {
                    await Api.instance.orderPreparing
                        .orderPreparingAssignPackagingToMePost(
                            assignRequest:
                                AssignRequest(orderId: widget.orderId));
                    getOrderContents();
                  },
                ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: IconButton(
                  onPressed: () async {
                    var checked = orderContentsList
                        .every((element) => element.product.delete);
                    if (checked) {
                      await Api.instance.orderPreparing
                          .orderPreparingReadyForDeliveryPost(
                              assignRequest:
                                  AssignRequest(orderId: widget.orderId));
                      return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(
                              "Status!",
                              style: TextStyle(color: Colors.redAccent),
                            ),
                            content: Text(
                              "The order ${widget.orderId} has been packaged!",
                            ),
                            actions: <Widget>[
                              new TextButton(
                                child: new Text(
                                  "OK",
                                ),
                                onPressed: () {
                                  Navigator.pushAndRemoveUntil(context,
                                      MaterialPageRoute(builder: (context) {
                                    return WarehouseFirst();
                                  }), (route) => false);
                                },
                              ),
                            ],
                          );
                        },
                      );
                    } else if (!checked) {
                      return showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text(
                              "Status!",
                              style: TextStyle(color: Colors.redAccent),
                            ),
                            content: Text(
                              "The order ${widget.orderId} is not packageable yet! Please scan and check the barcode of every item.",
                            ),
                            actions: <Widget>[
                              new TextButton(
                                child: new Text(
                                  "OK",
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    }
                  },
                  icon: Icon(
                    Icons.done_rounded,
                  ),
                  color: Colors.white,
                ),
              ),
            ],
            backgroundColor: Colors.black54,
            /*leading: TextButton.icon(
            onPressed: () {},
            icon: Icon(
              Icons.qr_code_scanner_rounded,
            ),
            label: Text("Scan"),
          ),*/
          ),
          body: ListView.builder(
            controller: ScrollController(),
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: orderContentsList.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 100,
                child: Card(
                  semanticContainer: false,
                  elevation: 20,
                  child: CheckboxListTile(
                      shape: CircleBorder(),
                      activeColor: Colors.pink[300],
                      dense: true,
                      subtitle: Html(
                        data: orderContentsList[index].product.shortDescription,
                      ),
                      //font change
                      title: new Text(
                        "${orderContentsList[index].productNameCache} X ${orderContentsList[index].quantity}",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            letterSpacing: 0.5),
                      ),
                      value: orderContentsList[index].product.delete,
                      secondary: Container(
                          height: 60,
                          width: 60,
                          child: Image.network(
                            orderContentsList[index].product.images.length > 0
                                ? Api.instance.client.basePath +
                                    "/cdn/image/150x150" +
                                    orderContentsList[index]
                                        .product
                                        .images
                                        .first
                                        .path
                                : Api.instance.client.basePath +
                                    "/cdn/image/150x150/d97a52df17a34e50b6c8b41ffb14c01a.png/no-image.png",
                            fit: BoxFit.contain,
                          )
                          /*orderContentsList[index]
                                  .product
                                  .images[0]
                                  .path*/
                          ),
                      onChanged: (bool val) {
                        if (order.packagingAgentId != globalWarehouseUser.id) {
                          snackBar("First you must accept this order!");
                          return;
                        }
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ScanBarcode(
                              productId: orderContentsList[index].product.id,
                              productBarcode: orderContentsList[index]
                                          .productBarcodeCache !=
                                      null
                                  ? orderContentsList[index].productBarcodeCache
                                  : orderContentsList[index].product.barcode !=
                                          null
                                      ? orderContentsList[index].product.barcode
                                      : "",
                              validated: () {
                                this.itemChange(true, index);
                              },
                            ),
                          ),
                        );
                        //itemChange(val, index);
                      }),
                ),
              );
            },
          ),
        ),
      );
    }
  }

  void itemChange(bool val, int index) {
    try {
      setState(() {
        orderContentsList[index].product.delete = val;
      });
    } catch (e) {
      snackBar("Unknown Problem. Please try again");
    }
  }

  snackBar(String message) {
    return ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: Duration(seconds: 2),
        backgroundColor: Colors.red,
      ),
    );
  }
}
