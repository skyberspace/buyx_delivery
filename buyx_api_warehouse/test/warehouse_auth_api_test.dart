//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

import 'package:buyx_api_warehouse/api.dart';
import 'package:test/test.dart';


/// tests for WarehouseAuthApi
void main() {
  final instance = WarehouseAuthApi();

  group('tests for WarehouseAuthApi', () {
    // returns true if OTP login is enabled for warehouse logins  if enabled LoginWithOTP -> LoginWithOTPStep2 methods should be used to login  if not Login method can be used
    //
    //Future<BooleanBR> warehouseAuthGetOTPLoginStatusPost() async
    test('test warehouseAuthGetOTPLoginStatusPost', () async {
      // TODO
    });

    //Future<AuthTokenBR> warehouseAuthGetUserFromTokenPost() async
    test('test warehouseAuthGetUserFromTokenPost', () async {
      // TODO
    });

    //Future<AuthTokenBR> warehouseAuthLoginPost({ String phone, String password }) async
    test('test warehouseAuthLoginPost', () async {
      // TODO
    });

    // Login and get a otp code for 2fa  returned string is the OTP request code and must be provided to LoginWithOTPStep2 action with 6 digit OTP for login
    //
    //Future<StringBR> warehouseAuthLoginWithOTPPost({ String phone, String password }) async
    test('test warehouseAuthLoginWithOTPPost', () async {
      // TODO
    });

    // OTP Login check otp code
    //
    //Future<AuthTokenBR> warehouseAuthLoginWithOTPStep2Post({ String code, String OTP }) async
    test('test warehouseAuthLoginWithOTPStep2Post', () async {
      // TODO
    });

    //Future<BR> warehouseAuthLogoutPost({ String token }) async
    test('test warehouseAuthLogoutPost', () async {
      // TODO
    });

  });
}
