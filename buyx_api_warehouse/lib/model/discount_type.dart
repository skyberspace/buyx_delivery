//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     1 = Flat    2 = Percentage
class DiscountType {
  /// Instantiate a new enum with the provided [value].
  const DiscountType._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number1 = DiscountType._(1);
  static const number2 = DiscountType._(2);

  /// List of all possible values in this [enum][DiscountType].
  static const values = <DiscountType>[
    number1,
    number2,
  ];

  static DiscountType fromJson(dynamic value) =>
    DiscountTypeTypeTransformer().decode(value);

  static List<DiscountType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <DiscountType>[]
      : json
          .map((value) => DiscountType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [DiscountType] to int,
/// and [decode] dynamic data back to [DiscountType].
class DiscountTypeTypeTransformer {
  const DiscountTypeTypeTransformer._();

  factory DiscountTypeTypeTransformer() => _instance ??= DiscountTypeTypeTransformer._();

  int encode(DiscountType data) => data.value;

  /// Decodes a [dynamic value][data] to a DiscountType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  DiscountType decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 1: return DiscountType.number1;
      case 2: return DiscountType.number2;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [DiscountTypeTypeTransformer] instance.
  static DiscountTypeTypeTransformer _instance;
}
