//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     1 = Equals    2 = LessThan    3 = BiggerThan    4 = NotEqual    5 = LessThanOrEqual    6 = BiggerThanOrEqual
class CartCampaignOperation {
  /// Instantiate a new enum with the provided [value].
  const CartCampaignOperation._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number1 = CartCampaignOperation._(1);
  static const number2 = CartCampaignOperation._(2);
  static const number3 = CartCampaignOperation._(3);
  static const number4 = CartCampaignOperation._(4);
  static const number5 = CartCampaignOperation._(5);
  static const number6 = CartCampaignOperation._(6);

  /// List of all possible values in this [enum][CartCampaignOperation].
  static const values = <CartCampaignOperation>[
    number1,
    number2,
    number3,
    number4,
    number5,
    number6,
  ];

  static CartCampaignOperation fromJson(dynamic value) =>
    CartCampaignOperationTypeTransformer().decode(value);

  static List<CartCampaignOperation> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CartCampaignOperation>[]
      : json
          .map((value) => CartCampaignOperation.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [CartCampaignOperation] to int,
/// and [decode] dynamic data back to [CartCampaignOperation].
class CartCampaignOperationTypeTransformer {
  const CartCampaignOperationTypeTransformer._();

  factory CartCampaignOperationTypeTransformer() => _instance ??= CartCampaignOperationTypeTransformer._();

  int encode(CartCampaignOperation data) => data.value;

  /// Decodes a [dynamic value][data] to a CartCampaignOperation.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  CartCampaignOperation decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 1: return CartCampaignOperation.number1;
      case 2: return CartCampaignOperation.number2;
      case 3: return CartCampaignOperation.number3;
      case 4: return CartCampaignOperation.number4;
      case 5: return CartCampaignOperation.number5;
      case 6: return CartCampaignOperation.number6;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [CartCampaignOperationTypeTransformer] instance.
  static CartCampaignOperationTypeTransformer _instance;
}
