//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

///     0 = Warehouse    1 = Restaurant    -1 = All
class OrderType {
  /// Instantiate a new enum with the provided [value].
  const OrderType._(this.value);

  /// The underlying value of this enum member.
  final int value;

  @override
  String toString() => value.toString();

  int toJson() => value;

  static const number0 = OrderType._(0);
  static const number1 = OrderType._(1);
  static const numberNegative1 = OrderType._(-1);

  /// List of all possible values in this [enum][OrderType].
  static const values = <OrderType>[
    number0,
    number1,
    numberNegative1,
  ];

  static OrderType fromJson(dynamic value) =>
    OrderTypeTypeTransformer().decode(value);

  static List<OrderType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <OrderType>[]
      : json
          .map((value) => OrderType.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [OrderType] to int,
/// and [decode] dynamic data back to [OrderType].
class OrderTypeTypeTransformer {
  const OrderTypeTypeTransformer._();

  factory OrderTypeTypeTransformer() => _instance ??= OrderTypeTypeTransformer._();

  int encode(OrderType data) => data.value;

  /// Decodes a [dynamic value][data] to a OrderType.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  OrderType decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case 0: return OrderType.number0;
      case 1: return OrderType.number1;
      case -1: return OrderType.numberNegative1;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [OrderTypeTypeTransformer] instance.
  static OrderTypeTypeTransformer _instance;
}
