# buyx_api_warehouse.model.AssignToOtherRequest

## Load the model package
```dart
import 'package:buyx_api_warehouse/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **int** |  | [optional] 
**warehouseUserId** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


