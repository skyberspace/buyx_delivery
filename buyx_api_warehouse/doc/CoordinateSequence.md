# buyx_api_warehouse.model.CoordinateSequence

## Load the model package
```dart
import 'package:buyx_api_warehouse/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dimension** | **int** |  | [optional] [readonly] 
**measures** | **int** |  | [optional] [readonly] 
**spatial** | **int** |  | [optional] [readonly] 
**ordinates** | [**Ordinates**](Ordinates.md) |  | [optional] 
**hasZ** | **bool** |  | [optional] [readonly] 
**hasM** | **bool** |  | [optional] [readonly] 
**zOrdinateIndex** | **int** |  | [optional] [readonly] 
**mOrdinateIndex** | **int** |  | [optional] [readonly] 
**count** | **int** |  | [optional] [readonly] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


